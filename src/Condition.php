<?php

namespace Cetria\Laravel\Filter;

use Cetria\Laravel\Filter\OperatorHandlers\Like;
use Cetria\Laravel\Filter\OperatorHandlers\NotIn;
use Cetria\Laravel\Filter\OperatorHandlers\Numeric;
use Cetria\Laravel\Filter\OperatorHandlers\Scope;
use Exception;
use Cetria\Helpers\Reflection\Reflection;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Cetria\Laravel\Filter\OperatorHandlers\In;
use Cetria\Laravel\Filter\OperatorHandlers\IsNull;
use Cetria\Laravel\Filter\OperatorHandlers\IsNotNull;
use Cetria\Laravel\Filter\Eloquent\Relations\FilterableInterface;
use Cetria\Laravel\Filter\OperatorHandlers\OrderBy;

class Condition
implements FilterEntity
{
    const SEPARATOR = '_;_';
    const ARRAY_SEPARATOR = '_,_';

    protected $logicalSeparator;
    protected $column;
    protected $operator;
    protected $value;

    /**
     * @see \Cetria\Laravel\Filter\Tests\Condition\ConstructorTest
     */
    public function __construct(string $columnOrQuery, ?Operator $operator = null, mixed $value = null, string $logicalSeparator= 'AND')
    {
        if(is_null($operator)) {
            $this->bootFromQuery($columnOrQuery);
        } else {
            $this->column = $columnOrQuery;
            $this->operator = $operator;
            $this->value = $value;
            $this->logicalSeparator = $logicalSeparator;
            $this->setValue($value);
        }
    }

    private function setValue(mixed $value): void
    {
        if(
            in_array($this->operator->name, [Operator::IN->name, Operator::NOT_IN->name, Operator::SCOPE->name])
                && !is_array($value)
        ) {
            $value = trim((string)$value, '{}');
            $this->value = explode(static::ARRAY_SEPARATOR, $value);
        } else {
            $this->value = $value;
        }
    }

    private function bootFromQuery(string $query): void
    {
        $parts = explode(static::SEPARATOR, $query);
        if(count($parts) == 4) {
            $this->logicalSeparator = $parts[0];
            $this->column = $parts[1];
            $this->operator = constant(Operator::class . '::' . $parts[2]);
            $this->setValue($parts[3]);
        } else {
            throw new Exception('Failed parse query string:\'' . $query . '\'');
        }
    }

    /**
     * @see \Cetria\Laravel\Filter\Tests\Condition\ConstructorTest ::fromQueryString()
     */
    public function __toString(): string
    {
        return $this->logicalSeparator . self::SEPARATOR . $this->column . self::SEPARATOR . $this->operator->name . self::SEPARATOR . $this->getValueAsString();
    }

    private function getValueAsString(): string
    {
        if(is_array($this->value)) {
            return '{' . implode(static::ARRAY_SEPARATOR, $this->value) . '}';
        } else {
            return (string) $this->value;
        }
    }

    /**
     * @see \Cetria\Laravel\Filter\Tests\Filter\FeatureInApplyTest
     * @see \Cetria\Laravel\Filter\Tests\Filter\FeatureIsNotNullApplyTest
     * @see \Cetria\Laravel\Filter\Tests\Filter\FeatureIsNullApplyTest
     * @see \Cetria\Laravel\Filter\Tests\Filter\FeatureLikeApplyTest
     * @see \Cetria\Laravel\Filter\Tests\Filter\FeatureNotInApplyTest
     * @see \Cetria\Laravel\Filter\Tests\Filter\FeatureOrderByApplyTest
     */
    public function apply(Builder|Collection|FilterableInterface &$builder): Builder|Collection|FilterableInterface
    {
        $this->throwIfApplyCallIndepently($builder);
        if(
            !(
                $builder instanceof Builder
                || $builder instanceof FilterableInterface
            ) && $this->logicalSeparator != 'AND'
        ) {
            throw new Exception('Condition with logical separator \'' . $this->logicalSeparator . '\' can not use for \'' . get_class($builder) . '\'');
        }
        $operatorHandler = $this->getOperatorHandler($this->operator);
         $operatorHandler->handle(
            $builder,
            column: $this->column,
            value:$this->value,
            operator: $this->operator->value,
            logicalSeparator: $this->logicalSeparator
        );
        return $builder;
    }

    private function throwIfApplyCallIndepently(Builder|Collection|FilterableInterface $builder): void
    {
        if(
            $builder instanceof FilterableInterface
                && !Reflection::isMethodCalledFromClassOrObject(Filter::class, 'apply')
        ) {
            throw new Exception('\'' . self::class . '::' . __FUNCTION__ . '\' method should not be called independently; it must be invoked using by \'' . Filter::class . '::apply\'');
        }
    }

    private function getOperatorHandler(Operator $operator): \Cetria\Laravel\Filter\OperatorHandlers\Operator
    {
        $operatorList = [
            IsNull::class,
            IsNotNull::class,
            In::class,
            Like::class,
            NotIn::class,
            Numeric::class,
            Scope::class,
            OrderBy::class,
        ];
        foreach($operatorList as $operatorClass) {
            /** @var \Cetria\Laravel\Filter\OperatorHandlers\Operator $operatorInstance */
            $operatorInstance = new $operatorClass();
            if($operatorInstance->isHandleable($operator)) {
                return $operatorInstance;
            }
        }
    }

    /**
     * @see \Cetria\Laravel\Filter\Tests\Condition\GetLogicalOperatorTest
     */
    public function getLogicalOperator(): string
    {
        return $this->logicalSeparator;
    }

    public function getOperatorAttribute(): Operator
    {
        return $this->operator;
    }
}
