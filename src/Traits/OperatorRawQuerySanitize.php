<?php

namespace Cetria\Laravel\Filter\Traits;

use Illuminate\Database\Eloquent\Builder;
use Cetria\Laravel\Filter\Eloquent\Relations\FilterableInterface;

trait OperatorRawQuerySanitize
{
    private function validateAndSanitizeColumnName(string $column, Builder|FilterableInterface $builder): string
    {
        $table = $builder->getModel()->getTable();
        $schema = $builder->getConnection()->getSchemaBuilder();
        if (!$schema->hasColumn($table, $column)) {
            throw new \InvalidArgumentException("Invalid column name string: $column");
        }

        // Escape backticks by replacing ` with ``
        $column = str_replace('`', '``', $column);

        // Wrap in backticks to safely use in queries
        $column = "`" . $column . "`";

        return $column;
    }
}