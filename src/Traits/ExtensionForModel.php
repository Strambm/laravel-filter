<?php

namespace Cetria\Laravel\Filter\Traits;

use Cetria\Laravel\Filter\Builder;
use Illuminate\Database\Eloquent\Model;
use Cetria\Laravel\Filter\Eloquent\Relations\HasOne;
use Cetria\Laravel\Filter\Eloquent\Relations\HasMany;
use Cetria\Laravel\Filter\Eloquent\Relations\MorphTo;
use Cetria\Laravel\Filter\Eloquent\Relations\MorphOne;
use Cetria\Laravel\Filter\Eloquent\Relations\BelongsTo;
use Cetria\Laravel\Filter\Eloquent\Relations\MorphMany;
use Cetria\Laravel\Filter\Eloquent\Relations\MorphToMany;
use Cetria\Laravel\Filter\Eloquent\Relations\BelongsToMany;
use Cetria\Laravel\Filter\Eloquent\Relations\HasOneThrough;
use Cetria\Laravel\Filter\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Builder as LaravelEloquentBuilder;

trait ExtensionForModel
{
    /**
     * Create a new Eloquent query builder for the model.
     *
     * @param  \Illuminate\Database\Query\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder|static
     * @see \Cetria\Laravel\Filter\Tests\Traits\ExtensionForModel\NewEloquentBuilderTest
     */
    public function newEloquentBuilder($query)
    {
        return new Builder($query);
    }

    /**
     * Instantiate a new HasOne relationship.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  \Illuminate\Database\Eloquent\Model  $parent
     * @param  string  $foreignKey
     * @param  string  $localKey
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     * @see \Cetria\Laravel\Filter\Tests\Traits\ExtensionForModel\NewHasOneTest
     */
    protected function newHasOne(LaravelEloquentBuilder $query, Model $parent, $foreignKey, $localKey)
    {
        return new HasOne($query, $parent, $foreignKey, $localKey);
    }

    /**
     * Instantiate a new HasOneThrough relationship.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  \Illuminate\Database\Eloquent\Model  $farParent
     * @param  \Illuminate\Database\Eloquent\Model  $throughParent
     * @param  string  $firstKey
     * @param  string  $secondKey
     * @param  string  $localKey
     * @param  string  $secondLocalKey
     * @return \Illuminate\Database\Eloquent\Relations\HasOneThrough
     * @see \Cetria\Laravel\Filter\Tests\Traits\ExtensionForModel\NewHasOneThroughTest
     */
    protected function newHasOneThrough(LaravelEloquentBuilder $query, Model $farParent, Model $throughParent, $firstKey, $secondKey, $localKey, $secondLocalKey)
    {
        return new HasOneThrough($query, $farParent, $throughParent, $firstKey, $secondKey, $localKey, $secondLocalKey);
    }

    /**
     * Instantiate a new MorphOne relationship.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  \Illuminate\Database\Eloquent\Model  $parent
     * @param  string  $type
     * @param  string  $id
     * @param  string  $localKey
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     * @see \Cetria\Laravel\Filter\Tests\Traits\ExtensionForModel\NewMorphOneTest
     */
    protected function newMorphOne(LaravelEloquentBuilder $query, Model $parent, $type, $id, $localKey)
    {
        return new MorphOne($query, $parent, $type, $id, $localKey);
    }

    /**
     * Instantiate a new BelongsTo relationship.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  \Illuminate\Database\Eloquent\Model  $child
     * @param  string  $foreignKey
     * @param  string  $ownerKey
     * @param  string  $relation
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * @see \Cetria\Laravel\Filter\Tests\Traits\ExtensionForModel\NewBelongsToTest
     */
    protected function newBelongsTo(LaravelEloquentBuilder $query, Model $child, $foreignKey, $ownerKey, $relation)
    {
        return new BelongsTo($query, $child, $foreignKey, $ownerKey, $relation);
    }

    /**
     * Instantiate a new MorphTo relationship.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  \Illuminate\Database\Eloquent\Model  $parent
     * @param  string  $foreignKey
     * @param  string  $ownerKey
     * @param  string  $type
     * @param  string  $relation
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     * @see \Cetria\Laravel\Filter\Tests\Traits\ExtensionForModel\NewMorphToTest
     */
    protected function newMorphTo(LaravelEloquentBuilder $query, Model $parent, $foreignKey, $ownerKey, $type, $relation)
    {
        return new MorphTo($query, $parent, $foreignKey, $ownerKey, $type, $relation);
    }

    /**
     * Instantiate a new HasMany relationship.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  \Illuminate\Database\Eloquent\Model  $parent
     * @param  string  $foreignKey
     * @param  string  $localKey
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * @see \Cetria\Laravel\Filter\Tests\Traits\ExtensionForModel\NewHasManyTest
     */
    protected function newHasMany(LaravelEloquentBuilder $query, Model $parent, $foreignKey, $localKey)
    {
        return new HasMany($query, $parent, $foreignKey, $localKey);
    }

    /**
     * Instantiate a new HasManyThrough relationship.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  \Illuminate\Database\Eloquent\Model  $farParent
     * @param  \Illuminate\Database\Eloquent\Model  $throughParent
     * @param  string  $firstKey
     * @param  string  $secondKey
     * @param  string  $localKey
     * @param  string  $secondLocalKey
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     * @see \Cetria\Laravel\Filter\Tests\Traits\ExtensionForModel\NewHasManyThroughTest
     */
    protected function newHasManyThrough(LaravelEloquentBuilder $query, Model $farParent, Model $throughParent, $firstKey, $secondKey, $localKey, $secondLocalKey)
    {
        return new HasManyThrough($query, $farParent, $throughParent, $firstKey, $secondKey, $localKey, $secondLocalKey);
    }

    /**
     * Instantiate a new MorphMany relationship.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  \Illuminate\Database\Eloquent\Model  $parent
     * @param  string  $type
     * @param  string  $id
     * @param  string  $localKey
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     * @see \Cetria\Laravel\Filter\Tests\Traits\ExtensionForModel\NewMorphManyTest
     */
    protected function newMorphMany(LaravelEloquentBuilder $query, Model $parent, $type, $id, $localKey)
    {
        return new MorphMany($query, $parent, $type, $id, $localKey);
    }

    /**
     * Instantiate a new BelongsToMany relationship.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  \Illuminate\Database\Eloquent\Model  $parent
     * @param  string  $table
     * @param  string  $foreignPivotKey
     * @param  string  $relatedPivotKey
     * @param  string  $parentKey
     * @param  string  $relatedKey
     * @param  string|null  $relationName
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     * @see \Cetria\Laravel\Filter\Tests\Traits\ExtensionForModel\NewBelongsToManyTest
     */
    protected function newBelongsToMany(LaravelEloquentBuilder $query, Model $parent, $table, $foreignPivotKey, $relatedPivotKey,
                                        $parentKey, $relatedKey, $relationName = null)
    {
        return new BelongsToMany($query, $parent, $table, $foreignPivotKey, $relatedPivotKey, $parentKey, $relatedKey, $relationName);
    }

    /**
     * Instantiate a new MorphToMany relationship.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  \Illuminate\Database\Eloquent\Model  $parent
     * @param  string  $name
     * @param  string  $table
     * @param  string  $foreignPivotKey
     * @param  string  $relatedPivotKey
     * @param  string  $parentKey
     * @param  string  $relatedKey
     * @param  string|null  $relationName
     * @param  bool  $inverse
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     * @see \Cetria\Laravel\Filter\Tests\Traits\ExtensionForModel\NewMorphToManyTest
     */
    protected function newMorphToMany(LaravelEloquentBuilder $query, Model $parent, $name, $table, $foreignPivotKey,
                                      $relatedPivotKey, $parentKey, $relatedKey,
                                      $relationName = null, $inverse = false)
    {
        return new MorphToMany($query, $parent, $name, $table, $foreignPivotKey, $relatedPivotKey, $parentKey, $relatedKey,
            $relationName, $inverse);
    }
}
