<?php

namespace Cetria\Laravel\Filter\Traits;

trait ExtensionForRelation
{
    use ExtensionForBuilder;

    public function where($column, $operator = null, $value = null, $boolean = 'and')
    {
        return $this->__call('where', [$column, $operator, $value, $boolean]);
    }
}
