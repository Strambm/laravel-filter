<?php

namespace Cetria\Laravel\Filter\Traits;

use Cetria\Laravel\Filter\FilterEntity;

trait ExtensionForBuilder
{
    /**
     * @see \Cetria\Laravel\Filter\Tests\Traits\ExtensionForBuilder\ApplyFilterTest
     */
    public function applyFilter(FilterEntity $filter): self
    {
        $filter->apply($this);
        return $this;
    }

    /**
     * Add a basic where clause to the query.
     *
     * @param  \Closure|string|array|\Illuminate\Contracts\Database\Query\Expression  $column
     * @param  mixed  $operator
     * @param  mixed  $value
     * @param  string  $boolean
     * @return $this
     * @see \Illuminate\Database\Eloquent\Builder
     */
    abstract public function where($column, $operator = null, $value = null, $boolean = 'and');
}
