<?php

namespace Cetria\Laravel\Filter;

use ValueError;

enum Operator: string
{
    case GT = '>';
    case GTE = '>=';
    case EQ = '=';
    case NEQ = '!=';
    case LTE = '<=';
    case LT = '<';
    case IN = 'IN';
    case NOT_IN = 'NOT IN';
    case LIKE = 'LIKE';
    case IS_NULL = 'IS NULL';
    case NOT_IS_NULL = 'N IS NULL';
    case SCOPE = 'SCOPE';
    case ORDER_BY = 'OB';

    /**
     * @see \Cetria\Laravel\Filter\Tests\Operator\GetValueFromNameTest
     */
    public static function getValueFromName(string $name): string
    {
        foreach (self::cases() as $status) {
            if( $name === $status->name ){
                return $status->value;
            }
        }
        throw new ValueError('\'' . $name . '\' is not a valid backing value for enum \'' . self::class . '\'');
    }
}
