<?php

namespace Cetria\Laravel\Filter\Eloquent\Relations;

use Cetria\Laravel\Filter\Traits\ExtensionForRelation;
use Illuminate\Database\Eloquent\Relations\HasOne as LaravelHasONe;


class HasOne extends LaravelHasOne implements FilterableInterface
{
    use ExtensionForRelation;
}
