<?php

namespace Cetria\Laravel\Filter\Eloquent\Relations;

use Cetria\Laravel\Filter\Traits\ExtensionForRelation;
use Illuminate\Database\Eloquent\Relations\HasManyThrough as LaravelHasManyThrough;

class HasManyThrough extends LaravelHasManyThrough implements FilterableInterface
{
    use ExtensionForRelation;
}
