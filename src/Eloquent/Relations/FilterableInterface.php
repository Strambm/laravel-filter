<?php

namespace Cetria\Laravel\Filter\Eloquent\Relations;

interface FilterableInterface
{
    public function where($column, $operator = null, $value = null, $boolean = 'and');
}
