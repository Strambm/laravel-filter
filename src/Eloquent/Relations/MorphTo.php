<?php

namespace Cetria\Laravel\Filter\Eloquent\Relations;

use Cetria\Laravel\Filter\Traits\ExtensionForRelation;
use Illuminate\Database\Eloquent\Relations\MorphTo as LaravelMorphTo;

class MorphTo extends LaravelMorphTo implements FilterableInterface
{
    use ExtensionForRelation;
}
