<?php

namespace Cetria\Laravel\Filter\Eloquent\Relations;

use Cetria\Laravel\Filter\Traits\ExtensionForRelation;
use Illuminate\Database\Eloquent\Relations\HasMany as LaravelHasMany;

class HasMany extends LaravelHasMany implements FilterableInterface
{
    use ExtensionForRelation;
}
