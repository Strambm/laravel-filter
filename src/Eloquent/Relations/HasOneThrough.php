<?php

namespace Cetria\Laravel\Filter\Eloquent\Relations;

use Cetria\Laravel\Filter\Traits\ExtensionForRelation;
use Illuminate\Database\Eloquent\Relations\HasOneThrough as LaravelHasOneThrough;

class HasOneThrough extends LaravelHasOneThrough implements FilterableInterface
{
    use ExtensionForRelation;
}
