<?php

namespace Cetria\Laravel\Filter\Eloquent\Relations;

use Cetria\Laravel\Filter\Traits\ExtensionForRelation;
use Illuminate\Database\Eloquent\Relations\BelongsToMany as LaravelBelongsToMany;

class BelongsToMany extends LaravelBelongsToMany implements FilterableInterface
{
    use ExtensionForRelation;
}
