<?php

namespace Cetria\Laravel\Filter\Eloquent\Relations;

use Cetria\Laravel\Filter\Traits\ExtensionForRelation;
use Illuminate\Database\Eloquent\Relations\MorphOne as LaravelMorphOne;

class MorphOne extends LaravelMorphOne implements FilterableInterface
{
    use ExtensionForRelation;
}
