<?php

namespace Cetria\Laravel\Filter\Eloquent\Relations;

use Cetria\Laravel\Filter\Traits\ExtensionForRelation;
use Illuminate\Database\Eloquent\Relations\BelongsTo as LaravelBelongsTo;


class BelongsTo extends LaravelBelongsTo implements FilterableInterface
{
    use ExtensionForRelation;
}
