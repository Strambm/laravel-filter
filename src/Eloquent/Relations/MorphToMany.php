<?php

namespace Cetria\Laravel\Filter\Eloquent\Relations;

use Cetria\Laravel\Filter\Traits\ExtensionForRelation;
use Illuminate\Database\Eloquent\Relations\MorphToMany as LaravelMorphToMany;

class MorphToMany extends LaravelMorphToMany implements FilterableInterface
{
    use ExtensionForRelation;
}
