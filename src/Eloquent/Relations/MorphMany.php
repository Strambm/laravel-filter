<?php

namespace Cetria\Laravel\Filter\Eloquent\Relations;

use Cetria\Laravel\Filter\Traits\ExtensionForRelation;
use Illuminate\Database\Eloquent\Relations\MorphMany as LaravelMorphMany;

class MorphMany extends LaravelMorphMany implements FilterableInterface
{
    use ExtensionForRelation;
}
