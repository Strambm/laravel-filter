<?php

namespace Cetria\Laravel\Filter;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Cetria\Laravel\Filter\Eloquent\Relations\FilterableInterface;

interface FilterEntity
{
    public function apply(Builder|Collection|FilterableInterface &$builder): Builder|Collection|FilterableInterface;
    public function getLogicalOperator(): string;
}
