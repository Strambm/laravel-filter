<?php

namespace Cetria\Laravel\Filter;

use Cetria\Laravel\Filter\Eloquent\Relations\FilterableInterface;
use Cetria\Laravel\Filter\FilterEntity;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class Filter
implements FilterEntity
{
    const SEPARATOR = '_CON%_';

    protected $level = 1;
    protected $scopes = [];

    /**
     * @see \Cetria\Laravel\Filter\Tests\Filter\ConstructorTest
     */
    public function __construct(?string $query = null, int $level = 1)
    {
        $this->level = $level;
        if(!is_null($query)) {
            $this->bootFromQuery($query);
        }
    }

    private function bootFromQuery(string $query): void
    {
        $blocks = \explode(static::getBlockSeparator($this->level), $query);
        $nextLevel = $this->level + 1;
        foreach($blocks as $queryBLock) {
            if(static::isBlock($queryBLock, $nextLevel)) {
                $this->addScope(new static($queryBLock, $nextLevel));
            } else {
                $this->addScope(new Condition($queryBLock));
            }
        }
    }

    private static function isBlock(string $query, int $level): bool
    {
        $separator = static::getBlockSeparator($level);
        return str_contains($query, $separator);
    }

    /**
     * @see \Cetria\Laravel\Filter\Tests\Filter\GetBlockSeparatorTest
     */
    protected static function getBlockSeparator(int $level): string
    {
        return \str_replace('%', $level, self::SEPARATOR);
    }

    /**
     * @see \Cetria\Laravel\Filter\Tests\Filter\ConstructorTest ::fromQueryString()
     */
    public function __toString(): string
    {
        return implode(self::getBlockSeparator($this->level), $this->scopes);
    }

    /**
     * @see \Cetria\Laravel\Filter\Tests\Filter\AddScopeTest
     */
    public function addScope(FilterEntity $scope): self
    {
        if($scope instanceof self) {
            $scope->setLevel($this->level + 1);
        }
        $this->scopes[] = $scope;
        return $this;
    }

    /**
     * @see \Cetria\Laravel\Filter\Tests\Filter\AddScopeTest ::filter()
     */
    protected function setLevel(int $level): static
    {
        $this->level = $level;
        foreach($this->scopes as $scope) {
            if($scope instanceof self) {
                $scope->setLevel($this->level + 1);
            }
        }
        return $this;
    }

    /**
     * @see \Cetria\Laravel\Filter\Tests\Filter\ApplyTest
     * @see \Cetria\Laravel\Filter\Tests\Filter\FeatureInApplyTest
     * @see \Cetria\Laravel\Filter\Tests\Filter\FeatureIsNotNullApplyTest
     * @see \Cetria\Laravel\Filter\Tests\Filter\FeatureIsNullApplyTest
     * @see \Cetria\Laravel\Filter\Tests\Filter\FeatureLikeApplyTest
     * @see \Cetria\Laravel\Filter\Tests\Filter\FeatureNotInApplyTest
     * @see \Cetria\Laravel\Filter\Tests\Filter\FeatureOrderByApplyTest
     */
    public function apply(Builder|Collection|FilterableInterface &$builder): Builder|Collection|FilterableInterface
    {
        if(
            $this->level == 1
                && $builder instanceof FilterableInterface
        ) {
            if($this->scopes[0]->getOperatorAttribute()->name === Operator::ORDER_BY->name) {
                $this->scopes[0]->apply($builder);
            }
            $builder->where(function($builder): void {
                $this->applyScopeForQueryBuilder($builder);
            });
        } elseif(
            $builder instanceof Builder
            || $builder instanceof FilterableInterface
        ) {
            $this->applyScopeForQueryBuilder($builder);

        } elseif($builder instanceof Collection) {
            foreach($this->scopes as $scope) {
                $builder = $scope->apply($builder);
            }
        }

        return $builder;
    }

    private function applyScopeForQueryBuilder(Builder|FilterableInterface &$builder): Builder|FilterableInterface
    {
        foreach ($this->scopes as $scope) {
            if ($scope->getOperatorAttribute()->name === Operator::ORDER_BY->name) {
                $scope->apply($builder);
            }
        }

        $builder->where(function(Builder $query): void {
            foreach ($this->scopes as $scope) {
                if (
                    $scope instanceof Filter
                    || ($scope->getOperatorAttribute()->name !== Operator::ORDER_BY->name)
                ) {
                    $scope->apply($query);
                }
            }
        }, null, null, $this->getLogicalOperator());

        return $builder;
    }

    /**
     * @see \Cetria\Laravel\Filter\Tests\Filter\GetLogicalOperatorTest
     */
    public function getLogicalOperator(): string
    {
        if(count($this->scopes) == 0) {
            return 'AND';
        } else {
            return $this->scopes[0]->getLogicalOperator();
        }
    }

    public function getOperatorAttribute() {
        return $this->scopes[0]->getOperatorAttribute();
    }
}
