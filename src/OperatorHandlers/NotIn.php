<?php

namespace Cetria\Laravel\Filter\OperatorHandlers;

use Cetria\Laravel\Filter\Operator as EnumOperator;

class NotIn extends InOperators
{
    protected $operators = [EnumOperator::NOT_IN];

}
