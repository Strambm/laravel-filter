<?php

namespace Cetria\Laravel\Filter\OperatorHandlers;

use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Cetria\Laravel\Filter\Operator as EnumOperator;
use Cetria\Laravel\Filter\Eloquent\Relations\FilterableInterface;

abstract class Operator
{
    protected $operators = [];
    
    public function isHandleable(EnumOperator $operator): bool
    {
        $operators = array_map(function(EnumOperator $operator): string { return $operator->name; }, $this->operators);
        return in_array($operator->name, $operators);
    }

    protected function assertUsingOr(Builder|Collection|FilterableInterface $builder, string $logicalSeparator): void
    {
        if(strtolower($logicalSeparator) == 'or' && $builder instanceof Builder == false) {
            throw new Exception('Condition with logical separator \'' . $logicalSeparator . '\' can not use for \'' . get_class($builder) . '\'');
        }
    }

    abstract public function handle(Builder|Collection|FilterableInterface &$builder, string $column = '', string $logicalSeparator = 'and', $value = null, string $operator = ''): void;
}
