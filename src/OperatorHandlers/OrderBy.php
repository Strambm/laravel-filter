<?php

namespace Cetria\Laravel\Filter\OperatorHandlers;

use Cetria\Laravel\Filter\Operator as EnumOperator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Cetria\Laravel\Filter\Eloquent\Relations\FilterableInterface;
use Exception;


class OrderBy extends Operator
{
    protected $operators = [EnumOperator::ORDER_BY];

    /**
     * @see \Cetria\Laravel\Filter\Tests\OperatorHandlers\OrderBy\HandleTest
     */
    public function handle(Builder|Collection|FilterableInterface &$builder, string $column = '', string $logicalSeparator = 'and', $value = 'ASC', string $operator = ''): void
    {
        $value = strtoupper($value);
        $operator = strtoupper($operator);
        $this->validateValue($value);
        $this->validateLogicalSeparator($logicalSeparator);
        if($builder instanceof Collection) {
            if($value == 'ASC') {
                $builder = $builder->sortBy($column);
            } else {
                $builder = $builder->sortByDesc($column);
            }
        }
        elseif($builder instanceof Builder || $builder instanceof FilterableInterface) {
            $builder = $builder->orderBy($column, $value);
        }
    }

    public function validateLogicalSeparator(string $logicalSeparator) {
        if($logicalSeparator !== 'AND') throw new Exception("OrderBy logical seperator must be AND, '$logicalSeparator' given");
    }

    public function validateValue(string $value) {
        if(!in_array($value,['ASC','DESC'])) throw new Exception('OrderBy value string is invalid (must be ASC or DESC)');
    }
}