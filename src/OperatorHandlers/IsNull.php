<?php

namespace Cetria\Laravel\Filter\OperatorHandlers;

use Cetria\Laravel\Filter\Operator as EnumOperator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Cetria\Laravel\Filter\Eloquent\Relations\FilterableInterface;


class IsNull extends Operator
{
    protected $operators = [EnumOperator::IS_NULL];

    /**
     * @see \Cetria\Laravel\Filter\Tests\OperatorHandlers\IsNull\HandleTest
     */
    public function handle(Builder|Collection|FilterableInterface &$builder, string $column = '', string $logicalSeparator = 'and', $value = null, string $operator = ''): void
    {
        $this->assertUsingOr($builder, $logicalSeparator);
        if($builder instanceof Collection == false) {
            $builder = $builder->whereNull($column, $logicalSeparator, true);
        } else {
            $builder = $builder->filter(function(Model $item) use ($column): bool {
                return is_null($item->$column);
            });
        }
    }
}
