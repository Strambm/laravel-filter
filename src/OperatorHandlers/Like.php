<?php

namespace Cetria\Laravel\Filter\OperatorHandlers;

use Cetria\Laravel\Filter\Operator as EnumOperator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Cetria\Laravel\Filter\Eloquent\Relations\FilterableInterface;
use Cetria\Laravel\Filter\Traits\OperatorRawQuerySanitize;

class Like extends Operator
{
    use OperatorRawQuerySanitize;
    protected $operators = [EnumOperator::LIKE];

    /**
     * @see \Cetria\Laravel\Filter\Tests\OperatorHandlers\Like\HandleTest
     */
    public function handle(Builder|Collection|FilterableInterface &$builder, string $column = '', string $logicalSeparator = 'and', $value = null, string $operator = ''): void
    {
        $this->assertUsingOr($builder, $logicalSeparator);
        if($builder instanceof Collection == false) {
            $column = $this->validateAndSanitizeColumnName($column, $builder);
            $queryMethod = (mb_strtolower($logicalSeparator) == 'or') ? 'orWhereRaw' : 'whereRaw';
            $builder = $builder->$queryMethod("BINARY LOWER($column) LIKE ?", [mb_strtolower($value)]);
        } else {
            $builder = $builder->filter(function(Model $item) use ($value, $column): bool {
                $value = mb_strtolower($value);
                $comparedItem = $item->$column;
                if (is_string($comparedItem)) {
                    $comparedItem = mb_strtolower($item->$column);
                }
                $regex = '/' . str_replace('%', '.*', $value) . '/i';
                return preg_match($regex, $comparedItem);
            });
        }
    }
}
