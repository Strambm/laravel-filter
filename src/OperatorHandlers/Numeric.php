<?php

namespace Cetria\Laravel\Filter\OperatorHandlers;

use Cetria\Laravel\Filter\Operator as EnumOperator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Cetria\Laravel\Filter\Eloquent\Relations\FilterableInterface;
use Cetria\Laravel\Filter\Traits\OperatorRawQuerySanitize;

class Numeric extends Operator
{
    use OperatorRawQuerySanitize;
    protected $operators = [EnumOperator::EQ, EnumOperator::GT, EnumOperator::GTE, EnumOperator::LT, EnumOperator::LTE, EnumOperator::NEQ];

    /**
     * @see \Cetria\Laravel\Filter\Tests\OperatorHandlers\Numeric\HandleTest
     */
    public function handle(Builder|Collection|FilterableInterface &$builder, string $column = '', string $logicalSeparator = 'and', $value = null, string $operator = ''): void
    {
        $this->assertUsingOr($builder, $logicalSeparator);
        $EQ_operators = [EnumOperator::EQ->value, EnumOperator::NEQ->value];
        if(!in_array($operator, $EQ_operators)) {
            $builder = $builder->where($column, $operator, $value, $logicalSeparator);
        } else {
            if($builder instanceof Collection == false) {
                $this->handleOperatorCaseInsensitive($builder, $column, $logicalSeparator, $value, $operator);
            } else {
                $this->handleOperatorCaseInsensitive_Collection($builder, $column, $logicalSeparator, $value, $operator);
            }
        }
    }

    private function handleOperatorCaseInsensitive(Builder|FilterableInterface &$builder, string $column, string $logicalSeparator, $value, string $operator): void
    {
        $column = $this->validateAndSanitizeColumnName($column, $builder);
        $queryMethod = (mb_strtolower($logicalSeparator) == 'or') ? 'orWhereRaw' : 'whereRaw';
        if (is_string($value)) {
            $builder->$queryMethod("BINARY LOWER($column) $operator BINARY LOWER(?)", [mb_strtolower($value)]);
        } else {
            $builder->$queryMethod("$column $operator ?", [$value]);
        }
    }

    private function handleOperatorCaseInsensitive_Collection(Collection &$builder, string $column, string $logicalSeparator, $value, string $operator): void
    {
        $value = is_string($value) ? mb_strtolower($value) : $value;
        $builder = $builder->filter(function ($item) use ($column, $value, $operator) {
            $itemValue = $item[$column] ?? null;
            if (is_string($itemValue)) {
                $itemValue = mb_strtolower($itemValue);
            }

            switch ($operator) {
                case EnumOperator::EQ->value:
                    return $itemValue == $value;
                case EnumOperator::NEQ->value:
                    return $itemValue != $value;
                default:
                    throw new \InvalidArgumentException("Unsupported operator: $operator");
            }
        });
    }
}
