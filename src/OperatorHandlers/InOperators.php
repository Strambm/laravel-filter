<?php

namespace Cetria\Laravel\Filter\OperatorHandlers;

use Cetria\Laravel\Filter\Operator as EnumOperator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Cetria\Laravel\Filter\Eloquent\Relations\FilterableInterface;
use Cetria\Laravel\Filter\Traits\OperatorRawQuerySanitize;

abstract class InOperators extends Operator
{
    use OperatorRawQuerySanitize;

    protected $operators = [EnumOperator::IN, EnumOperator::NOT_IN];

    /**
     * @see \Cetria\Laravel\Filter\Tests\OperatorHandlers\In\HandleTest
     * @see \Cetria\Laravel\Filter\Tests\OperatorHandlers\NotIn\HandleTest
     */
    public function handle(Builder|Collection|FilterableInterface &$builder, string $column = '', string $logicalSeparator = 'and', $value = null, string $operator = ''): void
    {
        $this->assertUsingOr($builder, $logicalSeparator);
        $this->validateOperator($operator);
        if ($builder instanceof Collection == false) {
            $this->handleOperatorCaseInsensitive($builder, $column, $logicalSeparator, $value, $operator);
        } else {
            $this->handleOperatorCaseInsensitive_Collection($builder, $column, $logicalSeparator, $value, $operator);
        }
    }

    private function handleOperatorCaseInsensitive(Builder|FilterableInterface &$builder, string $column, string $logicalSeparator, $value, string $operator): void
    {
        $queryMethod = (strtolower($logicalSeparator) == 'or') ? 'orWhere' : 'where';
        $column = $this->validateAndSanitizeColumnName($column, $builder);

        // Split values into strings and numbers
        $stringValues = array_filter((array) $value, 'is_string');
        $numericValues = array_filter((array) $value, 'is_numeric');

        $builder->$queryMethod(function ($query) use ($column, $stringValues, $numericValues, $operator) {
            if (!empty($stringValues)) {
                // Handle string values with LOWER for case-insensitivity
                $placeholders = implode(',', array_fill(0, count($stringValues), '?'));
                $loweredValues = array_map('mb_strtolower', $stringValues);

                $query->whereRaw(
                    "BINARY LOWER($column) $operator ($placeholders)",
                    $loweredValues
                );
            }

            if (!empty($numericValues)) {
                // Handle numeric values directly
                $placeholders = implode(',', array_fill(0, count($numericValues), '?'));
                if($operator == EnumOperator::IN->value){
                    $query->orWhereRaw(
                        "$column $operator ($placeholders)",
                        $numericValues
                    );
                } else {
                    $query->whereRaw(
                        "$column $operator ($placeholders)",
                        $numericValues
                    );
                }
            }
        });
    }

    private function handleOperatorCaseInsensitive_Collection(Collection &$builder, string $column, string $logicalSeparator, $value, string $operator): void
    {
        $value = array_map(fn($item) => mb_strtolower($item), (array) $value);

        $builder = $builder->filter(function ($item) use ($column, $value, $operator) {
            if ($operator === EnumOperator::IN->value) {
                return in_array(mb_strtolower($item[$column]), $value);
            } elseif ($operator === EnumOperator::NOT_IN->value) {
                return !in_array(mb_strtolower($item[$column]), $value);
            }
        });
    }

    private function validateOperator($operator): void {
        $IN_operators = [EnumOperator::IN->value, EnumOperator::NOT_IN->value];
        if(!in_array($operator, $IN_operators)) {
            throw new \InvalidArgumentException("Unsupported operator: $operator");
        }
    }
}