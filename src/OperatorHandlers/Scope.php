<?php

namespace Cetria\Laravel\Filter\OperatorHandlers;

use Exception;
use Illuminate\Support\Arr;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Cetria\Laravel\Filter\Operator as EnumOperator;
use Cetria\Laravel\Filter\Eloquent\Relations\FilterableInterface;


class Scope extends Operator
{
    protected $operators = [EnumOperator::SCOPE];

    /**
     * @see \Cetria\Laravel\Filter\Tests\OperatorHandlers\Scope\HandleTest
     */
    public function handle(Builder|Collection|FilterableInterface &$builder, string $column = '', string $logicalSeparator = 'and', $value = null, string $operator = ''): void
    {
        if($builder instanceof Builder == false) {
            throw new Exception('Scope can not use for \'' . get_class($builder) . '\'');
        }
        $scopeName = $column;
        $values = Arr::wrap($value);
        $builder = $builder->where(function(Builder $builder) use ($scopeName, $values): void { $builder->$scopeName(...$values); }, null, null, $logicalSeparator);
    }
}
