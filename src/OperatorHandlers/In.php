<?php

namespace Cetria\Laravel\Filter\OperatorHandlers;

use Cetria\Laravel\Filter\Operator as EnumOperator;

class In extends InOperators
{
    protected $operators = [EnumOperator::IN];

}
