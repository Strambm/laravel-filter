<?php

namespace Cetria\Laravel\Filter;

use Cetria\Laravel\Filter\Traits\ExtensionForBuilder;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;

class Builder extends EloquentBuilder
{
    use ExtensionForBuilder;
}
