<?php

namespace Cetria\Laravel\Filter\Tests;

use PHPUnit\Framework\TestCase as TestCasePhpUnit;
use Cetria\Laravel\Filter\Filter;
use Cetria\Laravel\Filter\Condition;
use Illuminate\Database\Eloquent\Model;
use Cetria\Helpers\Reflection\Reflection;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Collection;
use Cetria\Laravel\Helpers\Test\Dummy\Product;
use Cetria\Laravel\Helpers\Test\Dummy\Category;
use Cetria\Laravel\Helpers\Test\Dummy\DummyHelper;
use Cetria\Laravel\Filter\Traits\ExtensionForModel;
use Cetria\Laravel\Filter\OperatorHandlers\Operator;
use Cetria\Laravel\Filter\Eloquent\Relations\BelongsToMany;
use Cetria\Laravel\Filter\Eloquent\Relations\FilterableInterface;
use Cetria\Laravel\Helpers\Test\Dummy\DatabaseDrivers\MySQL;

abstract class TestCase extends TestCasePhpUnit
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->initDB();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        DummyHelper::close();
    }

    private function initDB(): void
    {
        DummyHelper::init(databaseDriver: new MySQL(
            $_ENV['DB_HOST'],
            $_ENV['DB_PORT'],
            $_ENV['DB_DATABASE'],
            $_ENV['DB_USERNAME'],
            $_ENV['DB_PASSWORD']
        ));

        $builder = Model::getConnectionResolver()->connection()->getSchemaBuilder();
        $builder->table(
            'products',
            function(Blueprint $table): void
            {
                $table->string('name')->nullable()->change();
                $table->float('price')->nullable()->change();
            }
        );
    }

    protected function makeColection(array $data): Collection
    {
        $class = $this->getTestModelClass();
        $collection = new Collection();
        foreach($data as $row) {
            $collection->push(new $class($row));
        }
        return $collection;
    }

    protected function makeBuilder(array $data): Builder
    {
        $class = $this->getTestModelClass();
        foreach($data as $row) {
            $class::factory()->create($row);
        }
        return (new $class())->query();
    }

    protected function makeFilterable(array $data): FilterableInterface
    {
        $class = $this->getTestModelClass();

        $category = Category::factory()->create();

        $filterable = new class($class) extends Category
        {
            use ExtensionForModel;

            protected $table = 'categories';
            protected $relationClass = null;

            public function __construct(string $class) {
                $this->relationClass = $class;
            }

            public function products(): BelongsToMany
            {
                return $this->belongsToMany($this->relationClass, 'category_product', 'category_id', 'product_id');
            }
        };
        $filterable->id = $category->id;

        foreach($data as $row) {
            $product = Product::factory()->create($row);
            $product->categories()->attach($category);
        }

        return $filterable->products();
    }

    protected function getTestModelClass(): string
    {
        return Product::class;
    }

}