<?php

namespace Cetria\Laravel\Filter\Tests\Condition;

use Cetria\Helpers\Reflection\Reflection;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Cetria\Laravel\Filter\Condition;
use Cetria\Laravel\Filter\Operator;

class ConstructorTest extends TestCase
{
    #[Test]
    public function withParams(): void
    {
        $column = 'column';
        $operator = Operator::EQ;
        $value = 8;
        $separateOperator = 'or';
        $condition = new Condition($column, $operator, $value, $separateOperator);
        $this->assertEquals($column, Reflection::getHiddenProperty($condition, 'column'));
        $this->assertEquals($operator, Reflection::getHiddenProperty($condition, 'operator'));
        $this->assertEquals($value, Reflection::getHiddenProperty($condition, 'value'));
        $this->assertEquals($separateOperator, Reflection::getHiddenProperty($condition, 'logicalSeparator'));
    }

    #[Test]
    #[DataProvider('fromQueryStringDataProvider')]
    public function fromQueryString(string $column, Operator $operator, mixed $value, string $separateOperator): void
    {
        $conditionSource = new Condition($column, $operator, $value, $separateOperator);
        $conditionResult = new Condition((string) $conditionSource);
        $this->assertEquals($column, Reflection::getHiddenProperty($conditionResult, 'column'));
        $this->assertEquals($operator, Reflection::getHiddenProperty($conditionResult, 'operator'));
        if(is_array($value)) {
            $this->assertEquals(array_values($value), Reflection::getHiddenProperty($conditionResult, 'value'));
        } else {
            $this->assertEquals($value, Reflection::getHiddenProperty($conditionResult, 'value'));
        }
        $this->assertEquals($separateOperator, Reflection::getHiddenProperty($conditionResult, 'logicalSeparator'));
    }

    public static function fromQueryStringDataProvider(): array
    {
        return [
            [
                'column2',
                Operator::LT,
                11,
                'OR',
            ], [
                'column3',
                Operator::NOT_IN,
                [11, 12, 13, 'test' => 8],
                'AND',
            ],
        ];
    }
}
