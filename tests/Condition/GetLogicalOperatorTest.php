<?php

namespace Cetria\Laravel\Filter\Tests\Condition;

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Cetria\Laravel\Filter\Condition;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Filter\Operator;

class GetLogicalOperatorTest extends TestCase
{
    #[Test]
    #[DataProvider('getTestDataProvider')]
    public function getTest(string $expectedOperator): void
    {
        $condition = new Condition('name', Operator::EQ, 1, $expectedOperator);
        $this->assertEquals($expectedOperator, Reflection::getHiddenProperty($condition, 'logicalSeparator'));
        $this->assertEquals($expectedOperator, $condition->getLogicalOperator());
    }

    public static function getTestDataProvider(): array
    {
        return [
            [
                'OR'
            ], [
                'AND'
            ]
        ];
    }
}
