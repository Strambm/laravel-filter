<?php

namespace Cetria\Laravel\Filter\Tests\Traits\ExtensionForModel;

use PHPUnit\Framework\TestCase;
use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Filter\Traits\ExtensionForModel;
use Illuminate\Database\Eloquent\Relations\Relation;
use Cetria\Laravel\Filter\Traits\ExtensionForRelation;

abstract class NewRelationTestCase extends TestCase
{
    protected function act(mixed ...$params): Relation
    {
        $trait = new class() {
            use ExtensionForModel;
        };
        $methodName = $this->getTestMethodName();
        $method = Reflection::getHiddenMethod($trait, $methodName);
        return $method->invoke($trait, ...$params);
    }

    protected function assertCorrectRelation(string $expectedRelationClass, Relation $result): void
    {
        $this->assertContains(ExtensionForRelation::class, Reflection::classUsesRecursive($result));
        $this->assertInstanceOf($expectedRelationClass, $result);
    }

    protected abstract function getTestMethodName(): string;
}
