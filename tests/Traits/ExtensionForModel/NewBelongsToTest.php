<?php

namespace Cetria\Laravel\Filter\Tests\Traits\ExtensionForModel;

use PHPUnit\Framework\Attributes\Test;
use Cetria\Laravel\Helpers\Test\Dummy\Product;
use Cetria\Laravel\Filter\Eloquent\Relations\BelongsTo;

class NewBelongsToTest extends NewRelationTestCase
{
    #[Test]
    public function testMethod(): void
    {
        $model = new Product();
        $result = $this->act($model->newQuery(), $model, 'foreignKey', 'ownerKey', 'realtion');
        $this->assertCorrectRelation(BelongsTo::class, $result);
    }

    protected function getTestMethodName(): string
    {
        return 'newBelongsTo';
    }
}
