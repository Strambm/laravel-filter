<?php

namespace Cetria\Laravel\Filter\Tests\Traits\ExtensionForModel;

use PHPUnit\Framework\Attributes\Test;
use Cetria\Laravel\Helpers\Test\Dummy\Product;
use Cetria\Laravel\Filter\Eloquent\Relations\HasOne;

class NewHasOneTest extends NewRelationTestCase
{
    #[Test]
    public function testMethod(): void
    {
        $model = new Product();
        $result = $this->act($model->newQuery(), $model, 'key1', 'key2');
        $this->assertCorrectRelation(HasOne::class, $result);
    }

    protected function getTestMethodName(): string
    {
        return 'newHasOne';
    }
}
