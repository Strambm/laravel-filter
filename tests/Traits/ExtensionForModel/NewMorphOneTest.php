<?php

namespace Cetria\Laravel\Filter\Tests\Traits\ExtensionForModel;

use PHPUnit\Framework\Test;
use Cetria\Laravel\Helpers\Test\Dummy\Product;
use Cetria\Laravel\Filter\Eloquent\Relations\MorphOne;

class NewMorphOneTest extends NewRelationTestCase
{
    #[Test]
    public function testMethod(): void
    {
        $model = new Product();
        $result = $this->act($model->newQuery(), $model, $model::class, 'id', 'localKey');
        $this->assertCorrectRelation(MorphOne::class, $result);
    }

    protected function getTestMethodName(): string
    {
        return 'newMorphOne';
    }
}
