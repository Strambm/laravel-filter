<?php

namespace Cetria\Laravel\Filter\Tests\Traits\ExtensionForModel;

use PHPUnit\Framework\TestCase;
use Cetria\Laravel\Filter\Builder;
use PHPUnit\Framework\Attributes\Test;
use Illuminate\Database\Eloquent\Model;
use Cetria\Laravel\Filter\Traits\ExtensionForModel;

class NewEloquentBuilderTest extends TestCase
{
    #[Test]
    public function testMethod(): void
    {
        $myModel = $this->arrangeModel();
        $result = $myModel::query();
        $this->assertInstanceOf(Builder::class, $result);
    }

    protected function arrangeModel(): Model
    {
        $model = new class extends Model
        {
            use ExtensionForModel;
        };
        return $model;
    }
}
