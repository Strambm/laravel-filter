<?php

namespace Cetria\Laravel\Filter\Tests\Traits\ExtensionForModel;

use PHPUnit\Framework\Attributes\Test;
use Cetria\Laravel\Helpers\Test\Dummy\Product;
use Cetria\Laravel\Filter\Eloquent\Relations\HasMany;

class NewHasManyTest extends NewRelationTestCase
{
    #[Test]
    public function testMethod(): void
    {
        $model = new Product();
        $result = $this->act($model->newQuery(), $model, 'foreignKey', 'localKey');
        $this->assertCorrectRelation(HasMany::class, $result);
    }

    protected function getTestMethodName(): string
    {
        return 'newHasMany';
    }
}
