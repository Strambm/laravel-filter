<?php

namespace Cetria\Laravel\Filter\Tests\Traits\ExtensionForModel;

use PHPUnit\Framework\Attributes\Test;
use Cetria\Laravel\Helpers\Test\Dummy\Product;
use Cetria\Laravel\Filter\Eloquent\Relations\BelongsToMany;

class NewBelongsToManyTest extends NewRelationTestCase
{
    #[Test]
    public function testMethod(): void
    {
        $model = new Product();
        $result = $this->act($model->newQuery(), $model, 'table', 'foreignPivotKey', 'relatedPivotKey', 'parentKey', 'relatedKey', 'relationName');
        $this->assertCorrectRelation(BelongsToMany::class, $result);
    }

    protected function getTestMethodName(): string
    {
        return 'newBelongsToMany';
    }
}
