<?php

namespace Cetria\Laravel\Filter\Tests\Traits\ExtensionForModel;

use PHPUnit\Framework\Attributes\Test;
use Cetria\Laravel\Helpers\Test\Dummy\Product;
use Cetria\Laravel\Filter\Eloquent\Relations\HasManyThrough;

class NewHasManyThroughTest extends NewRelationTestCase
{
    #[Test]
    public function testMethod(): void
    {
        $model = new Product();
        $result = $this->act($model->newQuery(), $model, $model, 'firstKey', 'ownerKey', 'secondKey', 'localKey', 'secondLocalKey');
        $this->assertCorrectRelation(HasManyThrough::class, $result);
    }

    protected function getTestMethodName(): string
    {
        return 'newHasManyThrough';
    }
}
