<?php

namespace Cetria\Laravel\Filter\Tests\Traits\ExtensionForBuilder;

use Cetria\Laravel\Filter\Operator;
//use PHPUnit\Framework\TestCase;
use Cetria\Laravel\Filter\Tests\TestCase;
use Cetria\Laravel\Filter\Filter;
use Illuminate\Database\Connection;
use Cetria\Laravel\Filter\Condition;
use PHPUnit\Framework\Attributes\Test;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Cetria\Laravel\Filter\Traits\ExtensionForBuilder;
use Illuminate\Database\Query\Builder as QueryBuilder;

use PHPUnit\Framework\Attributes\DataProviderExternal;
use Cetria\Helpers\Reflection\Reflection;

class OperatorRawQuerySanitizeTestCase extends TestCase
{
    #[Test]
    public function BuilderValid(): void
    {
        $data = $this->generateData('price', 1, 5);
        $builder = $this->makeBuilder($data);
        $modelWithTrait = $this->getModelWithTrait();
        $columnName = 'price';
        $method = Reflection::getHiddenMethod($modelWithTrait, 'validateAndSanitizeColumnName');
        $sanitizedColumnName = $method->invokeArgs($modelWithTrait, [$columnName, $builder]);
        $this->assertEquals("`price`", $sanitizedColumnName );
    }

    #[Test]
    public function FilterableInterfaceValid(): void
    {
        $data = $this->generateData('price', 1, 5);
        $builder = $this->makeFilterable($data);
        $modelWithTrait = $this->getModelWithTrait();
        $columnName = 'price';
        $method = Reflection::getHiddenMethod($modelWithTrait, 'validateAndSanitizeColumnName');
        $sanitizedColumnName = $method->invokeArgs($modelWithTrait, [$columnName, $builder]);
        $this->assertEquals("`price`", $sanitizedColumnName );
    }

    #[Test]
    public function NonexistentColumn(): void
    {
        $data = $this->generateData('price', 1, 5);
        $builder = $this->makeBuilder($data);
        $modelWithTrait = $this->getModelWithTrait();
        $columnName = 'nonexistentColumn';
        $method = Reflection::getHiddenMethod($modelWithTrait, 'validateAndSanitizeColumnName');
        $this->expectException(\InvalidArgumentException::class);
        $sanitizedColumnName = $method->invokeArgs($modelWithTrait, [$columnName, $builder]);
    }

    #[Test]
    public function SqlInjection(): void
    {
        $data = $this->generateData('price', 1, 5);
        $builder = $this->makeBuilder($data);
        $modelWithTrait = $this->getModelWithTrait();
        $columnName = "price`; DROP TABLE products; --";
        $method = Reflection::getHiddenMethod($modelWithTrait, 'validateAndSanitizeColumnName');
        $this->expectException(\InvalidArgumentException::class);
        $sanitizedColumnName = $method->invokeArgs($modelWithTrait, [$columnName, $builder]);
    }

    private function getModelWithTrait(): object {
        $modelWithTrait = new class()
        {
            use \Cetria\Laravel\Filter\Traits\OperatorRawQuerySanitize;
        };

        return $modelWithTrait;
    }

    private function generateData(string $column, int $from, int $to): array
    {
        $result = [];
        for($counter = $from; $counter <= $to; $counter ++) {
            $result[] = [$column => $counter];
        }
        return $result;
    }
}
