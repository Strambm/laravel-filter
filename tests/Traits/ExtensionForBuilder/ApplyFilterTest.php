<?php

namespace Cetria\Laravel\Filter\Tests\Traits\ExtensionForBuilder;

use Cetria\Laravel\Filter\Operator;
use PHPUnit\Framework\TestCase;
use Cetria\Laravel\Filter\Filter;
use Illuminate\Database\Connection;
use Cetria\Laravel\Filter\Condition;
use PHPUnit\Framework\Attributes\Test;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Cetria\Laravel\Filter\Traits\ExtensionForBuilder;
use Illuminate\Database\Query\Builder as QueryBuilder;

class ApplyFilterTest extends TestCase
{
    #[Test]
    public function testMethod(): void
    {
        $trait = $this->arrangeTrait();

        $filter = new Filter();
        $filter->addScope(new Condition('test', Operator::GT, 8));

        $result = $trait->applyFilter($filter);

        $this->assertInstanceOf(get_class($trait), $result);
        $this->assertCount(1, $result->getQuery()->wheres);
    }

    protected function arrangeTrait(): Builder
    {
        $model = new class() extends Model
        {
            public static function resolveConnection($connection = null)
            {
                return new Connection(function() {});
            }
        };
        $builderClass = new class(new QueryBuilder(new Connection(function() {}))) extends Builder
        {
            use ExtensionForBuilder;
        };

        $builderClass->setModel($model);
        return $builderClass;
    }
}
