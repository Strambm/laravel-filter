<?php

namespace Cetria\Laravel\Filter\Tests\OperatorHandlers\Numeric;

use Cetria\Laravel\Filter\Operator;
use Cetria\Laravel\Filter\Condition;
use Cetria\Laravel\Filter\Tests\OperatorHandlers\DataProvider;

class AndDataProvider extends DataProvider
{
    public static function getData(): array
    {
        return [
            [
                [
                    new Condition('price', Operator::LTE, 11),
                    new Condition('price', Operator::GT, 7)
                ],
                static::generateData('price', 1, 15),
                4
            ],
            [
                [
                    new Condition('price', Operator::LT, 11),
                    new Condition('price', Operator::NEQ, 2)
                ],
                static::generateData('price', 1, 15),
                9
            ],
            [
                [
                    new Condition('price', Operator::EQ, 11),
                ],
                static::generateData('price', 1, 15),
                1
            ],
            [
                [
                    new Condition('price', Operator::GT, 5),
                    new Condition('price', Operator::LTE, 10)
                ],
                static::generateData('price', 1, 15),
                5
            ],
            ////////////////////////   EQ, NEQ string data (case-insensitive):   ////////////////////////
            [
                [
                    new Condition('name', Operator::EQ, 'Hello'),
                ],
                [
                    [
                        'name' => 'hello',
                    ],
                    [
                        'name' => 'Hello',
                    ],
                    [
                        'name' => 'Hello words!',
                    ],
                    [
                        'name' => null,
                    ],
                ],
                2
            ],
            [
                [
                    new Condition('name', Operator::EQ, 'žvýkačka'),
                ],
                [
                    [
                        'name' => 'žvýkačka',
                    ],
                    [
                        'name' => 'Žvýkačka',
                    ],
                    [
                        'name' => null,
                    ],
                ],
                2
            ],
            [
                [
                    new Condition('name', Operator::NEQ, 'Hello'),
                    new Condition('name', Operator::NEQ, 'test'),
                ],
                [
                    [
                        'name' => 'hello',
                    ],
                    [
                        'name' => 'Hello',
                    ],
                    [
                        'name' => 'Hello words!',
                    ],
                    [
                        'name' => 'test',
                    ],
                ],
                1
            ],
            [
                [
                    new Condition('name', Operator::NEQ, 'žvýkačka'),
                ],
                [
                    [
                        'name' => 'žvýkačka',
                    ],
                    [
                        'name' => 'Žvýkačka',
                    ],
                    [
                        'name' => 'notŽvýkačka',
                    ],
                ],
                1
            ]
        ];
    }
}
