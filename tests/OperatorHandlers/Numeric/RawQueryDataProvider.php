<?php

namespace Cetria\Laravel\Filter\Tests\OperatorHandlers\Numeric;

use Cetria\Laravel\Filter\Operator;
use Cetria\Laravel\Filter\Condition;
use Cetria\Laravel\Filter\Tests\OperatorHandlers\DataProvider;

class RawQueryDataProvider extends DataProvider
{
    public static function getData(): array
    {
        return [
            [
                [
                    new Condition('nonexistentColumn', Operator::EQ, 1),
                ],
                static::generateData('price', 1, 15),
                \InvalidArgumentException::class
            ],
        ];
    }
}
