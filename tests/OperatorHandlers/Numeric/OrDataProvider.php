<?php

namespace Cetria\Laravel\Filter\Tests\OperatorHandlers\Numeric;

use Cetria\Laravel\Filter\Operator;
use Cetria\Laravel\Filter\Condition;
use Cetria\Laravel\Filter\Tests\OperatorHandlers\DataProvider;

class OrDataProvider extends DataProvider
{
    public static function getData(): array
    {
        return [
            [
                [
                    new Condition('price', Operator::EQ, 5, 'OR'),
                    new Condition('price', Operator::GT, 11, 'OR')
                ],
                static::generateData('price', 1, 15),
                5
            ], [
                [
                    new Condition('price', Operator::NEQ, 5, 'OR'),
                    new Condition('price', Operator::LT, 15, 'OR')
                ],
                static::generateData('price', 1, 15),
                15
            ], [
                [
                    new Condition('price', Operator::GTE, 10, 'OR'),
                    new Condition('price', Operator::LTE, 5, 'OR')
                ],
                static::generateData('price', 1, 15),
                11
            ],
           ////////////////////////   EQ, NEQ string data (case-insensitive):   ////////////////////////
            [
                [
                    new Condition('name', Operator::EQ, 'Hello', 'OR'),
                    new Condition('name', Operator::NEQ, 'test', 'OR'),
                ],
                [
                    [
                        'name' => 'hello',
                    ],
                    [
                        'name' => 'Hello',
                    ],
                    [
                        'name' => 'notHello',
                    ],
                    [
                        'name' => 'test',
                    ],
                ],
                3
            ],
            [
                [
                    new Condition('name', Operator::EQ, 'žvýkačka', 'OR'),
                    new Condition('name', Operator::NEQ, 'Hello', 'OR'),
                ],
                [
                    [
                        'name' => 'žvýkačka',
                    ],
                    [
                        'name' => 'Žvýkačka',
                    ],
                    [
                        'name' => 'notŽvýkačka',
                    ],
                    [
                        'name' => 'hello',
                    ],
                ],
                3
            ],
            [
                [
                    new Condition('name', Operator::NEQ, 'žvýkačka', 'OR'),
                    new Condition('name', Operator::EQ, 'žvýkačka', 'OR'),
                ],
                [
                    [
                        'name' => 'žvýkačka',
                    ],
                    [
                        'name' => 'Žvýkačka',
                    ],
                    [
                        'name' => 'notEqualŽvýkačka',
                    ],
                    [
                        'name' => 'test',
                    ],
                ],
                4
            ]
        ];
    }
}
