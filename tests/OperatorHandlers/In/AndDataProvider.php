<?php

namespace Cetria\Laravel\Filter\Tests\OperatorHandlers\In;

use Cetria\Laravel\Filter\Operator;
use Cetria\Laravel\Filter\Condition;
use Cetria\Laravel\Filter\Tests\OperatorHandlers\DataProvider;

class AndDataProvider extends DataProvider
{
    public static function getData(): array
    {
        return [
            [
                [
                    new Condition('price', Operator::IN, [1,2,3,4,5,6,7,8,9,10]),
                    new Condition('price', Operator::IN, [5,6,7,8,9,10,11,12])
                ],
                static::generateData('price', 1, 15),
                6
            ], [
                [
                    new Condition('name', Operator::IN, ['a', 'b', 'c']),
                    new Condition('name', Operator::IN, ['b', 'd'])
                ], [
                    [
                        'name' => 'a',
                    ], [
                        'name' => 'b',
                    ], [
                        'name' => 'c',
                    ], [
                        'name' => 'd',
                    ]
                ],
                1
            ],
            [
                [
                    new Condition('name', Operator::IN, ['a', 1, 'c']),
                    new Condition('name', Operator::IN, ['a', 1])
                ], [
                    [
                        'name' => 'a',
                    ], [
                        'name' => 'b',
                    ], [
                        'name' => 'c',
                    ], [
                        'name' => 'd',
                    ], [
                        'name' => 1,
                    ]
                ],
                2 //1: a,1,c && 2: a,1 => a,1
            ],
        ];
    }
}
