<?php

namespace Cetria\Laravel\Filter\Tests\OperatorHandlers\In;

use Cetria\Laravel\Filter\Operator;
use Cetria\Laravel\Filter\Condition;
use Cetria\Laravel\Filter\Tests\OperatorHandlers\DataProvider;

class AndCaseInsensitiveDataProvider extends DataProvider
{
    public static function getData(): array
    {
        return [
            [
                [
                    new Condition('name', Operator::IN, ['a', 'b', 'c']),
                    new Condition('name', Operator::IN, ['B', 'd'])
                ],
                [
                    [
                        'name' => 'a',
                    ],
                    [
                        'name' => 'b',
                    ],
                    [
                        'name' => 'B',
                    ],
                    [
                        'name' => 'c',
                    ],
                    [
                        'name' => 'd',
                    ]
                ],
                2
            ],
            [
                [
                    new Condition('name', Operator::IN, ['a', 'B', 'c']),
                ],
                [
                    [
                        'name' => 'a',
                    ],
                    [
                        'name' => 'b',
                    ],
                    [
                        'name' => 'B',
                    ],
                    [
                        'name' => 'c',
                    ],
                    [
                        'name' => 'd',
                    ]
                ],
                4
            ],
            [
                [
                    new Condition('name', Operator::IN, ['abc']),
                ],
                [
                    [
                        'name' => 'abc',
                    ],
                    [
                        'name' => 'aBc',
                    ],
                    [
                        'name' => 'ABC',
                    ],
                ],
                3
            ],
            [
                [
                    new Condition('name', Operator::IN, ['a', 'b', 1, 2]),
                    new Condition('name', Operator::IN, [2, 'B']),
                ],
                [
                    [
                        'name' => 'a',
                    ],
                    [
                        'name' => 'B',
                    ],
                    [
                        'name' => 1,
                    ],
                    [
                        'name' => 2,
                    ],
                ],
                2
            ],
            [
                [
                    new Condition('name', Operator::IN, ['a', 'B', 'Ř', 1, 2]),
                    new Condition('name', Operator::IN, ['a', 'ř']),
                ],
                [
                    [
                        'name' => 'a',
                    ],
                    [
                        'name' => 'á',
                    ],
                    [
                        'name' => 'r',
                    ],
                    [
                        'name' => 'ř',
                    ],
                    [
                        'name' => 1,
                    ],
                ],
                2
            ]
        ];
    }
}