<?php

namespace Cetria\Laravel\Filter\Tests\OperatorHandlers\In;

use Cetria\Laravel\Filter\Operator;
use Cetria\Laravel\Filter\Condition;
use Cetria\Laravel\Filter\Tests\OperatorHandlers\DataProvider;

class OrCaseInsensitiveDataProvider extends DataProvider
{
    public static function getData(): array
    {
        return [
            [
                [
                    new Condition('name', Operator::IN, ['a', 'c'], 'OR'),
                    new Condition('name', Operator::IN, ['b', 'd'], 'OR')
                ],
                [
                    [
                        'name' => 'a',
                    ],
                    [
                        'name' => 'b',
                    ],
                    [
                        'name' => 'B',
                    ],
                    [
                        'name' => 'c',
                    ],
                    [
                        'name' => 'd',
                    ]
                ],
                5
            ],
            [
                [
                    new Condition('name', Operator::IN, ['a', 'B', 'c'], 'OR'),
                    new Condition('name', Operator::IN, ['d'], 'OR'),
                ],
                [
                    [
                        'name' => 'a',
                    ],
                    [
                        'name' => 'b',
                    ],
                    [
                        'name' => 'B',
                    ],
                    [
                        'name' => 'c',
                    ],
                    [
                        'name' => 'd',
                    ]
                ],
                5
            ],
            [
                [
                    new Condition('name', Operator::IN, ['abc'], 'OR'),
                    new Condition('name', Operator::IN, ['AbC'], 'OR'),
                ],
                [
                    [
                        'name' => 'abc',
                    ],
                    [
                        'name' => 'ABC',
                    ],
                    [
                        'name' => 'abC',
                    ],
                    [
                        'name' => 'ABC',
                    ],
                ],
                4
            ],
            [
                [
                    new Condition('name', Operator::IN, ['A', 1, 2], 'OR'),
                    new Condition('name', Operator::IN, ['b', 1, 3], 'OR'),
                ],
                [
                    [
                        'name' => 'a',
                    ],
                    [
                        'name' => 'B',
                    ],
                    [
                        'name' => 1,
                    ],
                    [
                        'name' => 3,
                    ],
                    [
                        'name' => 4,
                    ],
                ],
                4
            ],
            [
                [
                    new Condition('name', Operator::IN, ['a', 'Ř'], 'OR'),
                    new Condition('name', Operator::IN, [1], 'OR'),
                ],
                [
                    [
                        'name' => 'a',
                    ],
                    [
                        'name' => 'á',
                    ],
                    [
                        'name' => 'r',
                    ],
                    [
                        'name' => 'ř',
                    ],
                    [
                        'name' => 'Ř',
                    ],
                    [
                        'name' => 1,
                    ],
                ],
                4 //(1: a, ř, Ř || 2: 1) => a, ř, Ř, 1
            ]
        ];
    }
}