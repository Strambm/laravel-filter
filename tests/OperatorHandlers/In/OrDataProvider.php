<?php

namespace Cetria\Laravel\Filter\Tests\OperatorHandlers\In;

use Cetria\Laravel\Filter\Operator;
use Cetria\Laravel\Filter\Condition;
use Cetria\Laravel\Filter\Tests\OperatorHandlers\DataProvider;

class OrDataProvider extends DataProvider
{
    public static function getData(): array
    {
        return [
            [
                [
                    new Condition('price', Operator::IN, [1,2,3,4,5,6,7,8,9,10], 'OR'),
                    new Condition('price', Operator::IN, [5,6,7,8,9,10,11,12], 'OR')
                ],
                static::generateData('price', 1, 15),
                12
            ], [
                [
                    new Condition('name', Operator::IN, ['a', 'b', 'c'], 'OR'),
                    new Condition('name', Operator::IN, ['b', 'd'], 'OR')
                ], [
                    [
                        'name' => 'a',
                    ], [
                        'name' => 'b',
                    ], [
                        'name' => 'c',
                    ], [
                        'name' => 'd',
                    ]
                ],
                4
            ], [
                [
                    new Condition('name', Operator::IN, ['a', 1, 'c'], 'OR'),
                    new Condition('name', Operator::IN, ['b',], 'OR')
                ], [
                    [
                        'name' => 'a',
                    ], [
                        'name' => 'b',
                    ], [
                        'name' => 'c',
                    ], [
                        'name' => 'd',
                    ], [
                        'name' => 1,
                    ]
                ],
                4 //(1: a,1,c || 2: b) => a,b,c,1
            ]
        ];
    }
}
