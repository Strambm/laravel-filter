<?php

namespace Cetria\Laravel\Filter\Tests\OperatorHandlers\Scope;

use Exception;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Laravel\Filter\OperatorHandlers\Scope;
use Cetria\Laravel\Filter\OperatorHandlers\Operator;
use PHPUnit\Framework\Attributes\DataProviderExternal;
use Cetria\Laravel\Filter\Tests\OperatorHandlers\Scope\Product;
use Cetria\Laravel\Filter\Tests\OperatorHandlers\HandleOperatorTestCase;

class HandleTest extends HandleOperatorTestCase
{
    #[Test]
    #[DataProviderExternal(AndDataProvider::class, 'getData')]
    public function andCollection(array $conditions, array $data, int $expectedCount): void
    {
        $this->expectException(Exception::class);
        parent::andCollection($conditions, $data, $expectedCount);
    }

    #[Test]
    #[DataProviderExternal(AndDataProvider::class, 'getData')]
    public function andBuilder(array $conditions, array $data, int $expectedCount): void
    {
        parent::andBuilder($conditions, $data, $expectedCount);
    }

    #[Test]
    #[DataProviderExternal(AndDataProvider::class, 'getData')]
    public function andFilterable(array $conditions, array $data, int $expectedCount): void
    {
        $this->expectException(Exception::class);
        parent::andFilterable($conditions, $data, $expectedCount);
    }

    #[Test]
    #[DataProviderExternal(OrDataProvider::class, 'getData')]
    public function orCollection(array $conditions, array $data, int $expectedCount): void
    {
        $this->expectException(Exception::class);
        parent::orCollection($conditions, $data, $expectedCount);
    }

    #[Test]
    #[DataProviderExternal(OrDataProvider::class, 'getData')]
    public function orBuilder(array $conditions, array $data, int $expectedCount): void
    {
        parent::orBuilder($conditions, $data, $expectedCount);
    }

    #[Test]
    #[DataProviderExternal(OrDataProvider::class, 'getData')]
    public function orFilterable(array $conditions, array $data, int $expectedCount): void
    {
        $this->expectException(Exception::class);
        parent::orFilterable($conditions, $data, $expectedCount);
    }

    protected function getOperatorHandler(): Operator
    {
        return new Scope();
    }

    protected function getTestModelClass(): string
    {
        return Product::class;
    }
}
