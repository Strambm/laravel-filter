<?php

namespace Cetria\Laravel\Filter\Tests\OperatorHandlers\Scope;

use Illuminate\Database\Eloquent\Builder;
use Cetria\Laravel\Helpers\Test\Dummy\Product as Dummy;

class Product extends Dummy
{
    public function scopeGtPrice(Builder $builder, float $value): void
    {
        $builder->where('price', '>', $value);
    }

    public function scopeOddPrice(Builder $builder): void
    {
        $builder->whereRaw('MOD(price, 2) = 1');
    }
}
