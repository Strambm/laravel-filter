<?php

namespace Cetria\Laravel\Filter\Tests\OperatorHandlers\Scope;

use Cetria\Laravel\Filter\Operator;
use Cetria\Laravel\Filter\Condition;
use Cetria\Laravel\Filter\Tests\OperatorHandlers\DataProvider;

class OrDataProvider extends DataProvider
{
    public static function getData(): array
    {
        return [
            [
                [
                    new Condition('gtPrice', Operator::SCOPE, [8], 'OR'),
                    new Condition('oddPrice', Operator::SCOPE, null, 'OR')
                ], 
                static::generateData('price', 1, 15),
                11
            ]
        ];
    }
}
