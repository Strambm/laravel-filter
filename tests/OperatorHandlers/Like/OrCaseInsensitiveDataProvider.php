<?php

namespace Cetria\Laravel\Filter\Tests\OperatorHandlers\Like;

use Cetria\Laravel\Filter\Operator;
use Cetria\Laravel\Filter\Condition;
use Cetria\Laravel\Filter\Tests\OperatorHandlers\DataProvider;

class OrCaseInsensitiveDataProvider extends DataProvider
{
    public static function getData(): array
    {
        return [
            [
                [
                    new Condition('name', Operator::LIKE, 'Hello%', 'OR'),
                    new Condition('name', Operator::LIKE, '%llo wo%', 'OR')
                ], [
                    [
                        'name' => 'Hello world!',
                    ], [
                        'name' => 'Hello!',
                    ], [
                        'name' => 'hello words!',
                    ],
                ],
                3
            ],
            [
                [
                    new Condition('name', Operator::LIKE, 'hello%', 'OR'),
                    new Condition('name', Operator::LIKE, '%Llo wo%', 'OR')
                ], [
                    [
                        'name' => 'Hello world!',
                    ], [
                        'name' => 'Hello!',
                    ], [
                        'name' => 'hello words!',
                    ],
                ],
                3
            ]
        ];
    }
}
