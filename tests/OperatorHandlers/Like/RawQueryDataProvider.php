<?php

namespace Cetria\Laravel\Filter\Tests\OperatorHandlers\Like;

use Cetria\Laravel\Filter\Operator;
use Cetria\Laravel\Filter\Condition;
use Cetria\Laravel\Filter\Tests\OperatorHandlers\DataProvider;

class RawQueryDataProvider extends DataProvider
{
    public static function getData(): array
    {
        return [
            [
                [
                    new Condition('nonexistentColumn', Operator::LIKE, 'Hello%'),
                ], [
                    [
                        'name' => 'Hello world!',
                    ], [
                        'name' => 'Hello!',
                    ], [
                        'name' => 'hello words!',
                    ],
                ],
                \InvalidArgumentException::class
            ],
        ];
    }
}
