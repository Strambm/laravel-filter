<?php

namespace Cetria\Laravel\Filter\Tests\OperatorHandlers\Like;

use Cetria\Laravel\Filter\OperatorHandlers\Like;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Laravel\Filter\OperatorHandlers\Operator;
use PHPUnit\Framework\Attributes\DataProviderExternal;
use Cetria\Laravel\Filter\Tests\OperatorHandlers\HandleOperatorTestCase;

class HandleTest extends HandleOperatorTestCase
{
    #[Test]
    #[DataProviderExternal(RawQueryDataProvider::class, 'getData')]
    public function rawQueryWithColumnSanitize_Builder(array $conditions, array $data, string $expectedExceptionClass): void
    {
        $builder = $this->makeBuilder($data);
        $this->expectException($expectedExceptionClass);
        $this->actWithOperator($builder, $conditions, $this->getOperatorHandler());
    }

    #[Test]
    #[DataProviderExternal(RawQueryDataProvider::class, 'getData')]
    public function rawQueryWithColumnSanitize_Filterable(array $conditions, array $data, string $expectedExceptionClass): void
    {
        $filterable = $this->makeFilterable($data);
        $this->expectException($expectedExceptionClass);
        $this->actWithOperator($filterable, $conditions, $this->getOperatorHandler());
    }

    #[Test]
    #[DataProviderExternal(AndDataProvider::class, 'getData')]
    public function andCollection(array $conditions, array $data, int $expectedCount): void
    {
        parent::andCollection($conditions, $data, $expectedCount);
    }

    #[Test]
    #[DataProviderExternal(AndDataProvider::class, 'getData')]
    public function andBuilder(array $conditions, array $data, int $expectedCount): void
    {
        parent::andBuilder($conditions, $data, $expectedCount);
    }

    #[Test]
    #[DataProviderExternal(AndDataProvider::class, 'getData')]
    public function andFilterable(array $conditions, array $data, int $expectedCount): void
    {
        parent::andFilterable($conditions, $data, $expectedCount);
    }

    #[Test]
    #[DataProviderExternal(OrDataProvider::class, 'getData')]
    public function orCollection(array $conditions, array $data, int $expectedCount): void
    {
        parent::orCollection($conditions, $data, $expectedCount);
    }

    #[Test]
    #[DataProviderExternal(OrDataProvider::class, 'getData')]
    public function orBuilder(array $conditions, array $data, int $expectedCount): void
    {
        parent::orBuilder($conditions, $data, $expectedCount);
    }

    #[Test]
    #[DataProviderExternal(OrDataProvider::class, 'getData')]
    public function orFilterable(array $conditions, array $data, int $expectedCount): void
    {
        parent::orFilterable($conditions, $data, $expectedCount);
    }

    #[Test]
    #[DataProviderExternal(AndCaseInsensitiveDataProvider::class, 'getData')]
    public function andCaseInsensitiveCollection(array $conditions, array $data, int $expectedCount): void
    {
        parent::andCollection($conditions, $data, $expectedCount);
    }

    #[Test]
    #[DataProviderExternal(AndCaseInsensitiveDataProvider::class, 'getData')]
    public function andCaseInsensitiveBuilder(array $conditions, array $data, int $expectedCount): void
    {
        parent::andBuilder($conditions, $data, $expectedCount);
    }

    #[Test]
    #[DataProviderExternal(AndCaseInsensitiveDataProvider::class, 'getData')]
    public function andCaseInsensitiveFilterable(array $conditions, array $data, int $expectedCount): void
    {
        parent::andFilterable($conditions, $data, $expectedCount);
    }

    #[Test]
    #[DataProviderExternal(OrCaseInsensitiveDataProvider::class, 'getData')]
    public function orCaseInsensitiveCollection(array $conditions, array $data, int $expectedCount): void
    {
        parent::orCollection($conditions, $data, $expectedCount);
    }

    #[Test]
    #[DataProviderExternal(OrCaseInsensitiveDataProvider::class, 'getData')]
    public function orCaseInsensitiveBuilder(array $conditions, array $data, int $expectedCount): void
    {
        parent::orBuilder($conditions, $data, $expectedCount);
    }

    #[Test]
    #[DataProviderExternal(OrCaseInsensitiveDataProvider::class, 'getData')]
    public function orCaseInsensitiveFilterable(array $conditions, array $data, int $expectedCount): void
    {
        parent::orFilterable($conditions, $data, $expectedCount);
    }

    protected function getOperatorHandler(): Operator
    {
        return new Like();
    }
}
