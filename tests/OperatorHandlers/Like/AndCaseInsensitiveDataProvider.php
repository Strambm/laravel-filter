<?php

namespace Cetria\Laravel\Filter\Tests\OperatorHandlers\Like;

use Cetria\Laravel\Filter\Operator;
use Cetria\Laravel\Filter\Condition;
use Cetria\Laravel\Filter\Tests\OperatorHandlers\DataProvider;

class AndCaseInsensitiveDataProvider extends DataProvider
{
    public static function getData(): array
    {
        return [
            [
                [
                    new Condition('name', Operator::LIKE, 'Hello%'),
                    new Condition('name', Operator::LIKE, '%llo wo%')
                ], [
                    [
                        'name' => 'Hello world!',
                    ], [
                        'name' => 'Hello!',
                    ], [
                        'name' => 'hello words!',
                    ],
                ],
                2
            ],
            [
                [
                    new Condition('name', Operator::LIKE, 'hello%'),
                    new Condition('name', Operator::LIKE, '%Llo wo%')
                ], [
                    [
                        'name' => 'Hello world!',
                    ], [
                        'name' => 'Hello!',
                    ], [
                        'name' => 'hello words!',
                    ],
                ],
                2
            ],
            [
                [
                    new Condition('name', Operator::LIKE, 'ře%'),
                    new Condition('name', Operator::LIKE, '%řich%')
                ], [
                    [
                        'name' => 'Řeřicha!',
                    ], [
                        'name' => 'řericha',
                    ], [
                        'name' => 'test',
                    ],
                ],
                1
            ],
            [
                [
                    new Condition('name', Operator::LIKE, 're%'),
                    new Condition('name', Operator::LIKE, '%řich%')
                ], [
                    [
                        'name' => 'rericha test2!',
                    ], [
                        'name' => 'Rericha test2',
                    ], [
                        'name' => 'test2',
                    ],
                ],
                0
            ]
        ];
    }
}
