<?php

namespace Cetria\Laravel\Filter\Tests\OperatorHandlers\Like;

use Cetria\Laravel\Filter\Operator;
use Cetria\Laravel\Filter\Condition;
use Cetria\Laravel\Filter\Tests\OperatorHandlers\DataProvider;

class AndDataProvider extends DataProvider
{
    public static function getData(): array
    {
        return [
            [
                [
                    new Condition('price', Operator::LIKE, '%55%'),
                    new Condition('price', Operator::LIKE, '15%')
                ], 
                static::generateData('price', 1550, 1570),
                10
            ], [
                [
                    new Condition('name', Operator::LIKE, 'Hello%'),
                    new Condition('name', Operator::LIKE, '%llo wo%')
                ], [
                    [
                        'name' => 'Hello world!',
                    ], [
                        'name' => 'Hello!',
                    ], [
                        'name' => 'Hello words!',
                    ],
                ],
                2
            ]
        ];
    }
}
