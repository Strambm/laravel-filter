<?php

namespace Cetria\Laravel\Filter\Tests\OperatorHandlers\Like;

use Cetria\Laravel\Filter\Operator;
use Cetria\Laravel\Filter\Condition;
use Cetria\Laravel\Filter\Tests\OperatorHandlers\DataProvider;

class OrDataProvider extends DataProvider
{
    public static function getData(): array
    {
        return [
            [
                [
                    new Condition('price', Operator::LIKE, '%55%', 'OR'),
                    new Condition('price', Operator::LIKE, '%0', 'OR')
                ],
                static::generateData('price', 1550, 1570),
                12
            ],[
                [
                    new Condition('name', Operator::LIKE, 'Hello%', 'OR'),
                    new Condition('name', Operator::LIKE, '%llo wo%', 'OR')
                ], [
                    [
                        'name' => 'Hello world!',
                    ], [
                        'name' => 'Hello!',
                    ], [
                        'name' => 'Hello words!',
                    ],
                ],
                3
            ]
        ];
    }
}
