<?php

namespace Cetria\Laravel\Filter\Tests\OperatorHandlers\IsNotNull;

use Cetria\Laravel\Filter\Operator;
use Cetria\Laravel\Filter\Condition;
use Cetria\Laravel\Filter\Tests\OperatorHandlers\DataProvider;

class OrDataProvider extends DataProvider
{
    public static function getData(): array
    {
        return [
            [
                [
                    new Condition('price', Operator::NOT_IS_NULL, null, 'OR'),
                    new Condition('name', Operator::NOT_IS_NULL, null, 'OR')
                ], [
                    [
                        'price' => null,
                        'name' => '11'
                    ], [
                        'price' => null,
                        'name' => null
                    ], [
                        'price' => 12,
                        'name' => 'gegr'
                    ], [
                        'price' => 1,
                        'name' => null
                    ]
                ],
                3
            ]
        ];
    }
}
