<?php

namespace Cetria\Laravel\Filter\Tests\OperatorHandlers;

abstract class DataProvider
{
    protected static function generateData(string $column, int $from, int $to): array
    {
        $result = [];
        for($counter = $from; $counter <= $to; $counter ++) {
            $result[] = [$column => $counter];
        }
        return $result;
    }
}
