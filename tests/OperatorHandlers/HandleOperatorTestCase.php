<?php

namespace Cetria\Laravel\Filter\Tests\OperatorHandlers;

use Cetria\Laravel\Filter\Tests\TestCase;
use Exception;
use Cetria\Laravel\Filter\Condition;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Cetria\Laravel\Filter\Eloquent\Relations\FilterableInterface;
use Cetria\Laravel\Filter\OperatorHandlers\Operator;
use Cetria\Helpers\Reflection\Reflection;



abstract class HandleOperatorTestCase extends TestCase
{
    /**
     * @param Condition[] $conditions
     */
    protected function actWithOperator(Builder|Collection|FilterableInterface &$source, array $conditions, Operator $operator): void
    {
        foreach($conditions as $condition) {
            $operator->handle(
                $source,
                Reflection::getHiddenProperty($condition, 'column'),
                Reflection::getHiddenProperty($condition, 'logicalSeparator'),
                Reflection::getHiddenProperty($condition, 'value'),
                Reflection::getHiddenProperty($condition, 'operator')->value
            );
        }
    }

    /**
     * @param Condition[] $conditions
     * @param array<string, mixed> $data
     */
    public function andCollection(array $conditions, array $data, int $expectedCount): void
    {
        $collection = $this->makeColection($data);
        $this->actWithOperator($collection, $conditions, $this->getOperatorHandler());
        $this->assertEquals($expectedCount, $collection->count());
    }

    /**
     * @param Condition[] $conditions
     * @param array<string, mixed> $data
     */
    public function andBuilder(array $conditions, array $data, int $expectedCount): void
    {
        $builder = $this->makeBuilder($data);
        $this->actWithOperator($builder, $conditions, $this->getOperatorHandler());
        $this->assertEquals($expectedCount, $builder->count());
    }

    /**
     * @param Condition[] $conditions
     * @param array<string, mixed> $data
     */
    public function andFilterable(array $conditions, array $data, int $expectedCount): void
    {
        $filterable = $this->makeFilterable($data);
        $this->actWithOperator($filterable, $conditions, $this->getOperatorHandler());
        $this->assertEquals($expectedCount, $filterable->count());
    }

    /**
     * @param Condition[] $conditions
     * @param array<string, mixed> $data
     */
    public function orCollection(array $conditions, array $data, int $expectedCount): void
    {
        $collection = $this->makeColection($data);
        $this->expectException(Exception::class);
        $this->actWithOperator($collection, $conditions, $this->getOperatorHandler());
    }

    /**
     * @param Condition[] $conditions
     * @param array<string, mixed> $data
     */
    public function orBuilder(array $conditions, array $data, int $expectedCount): void
    {
        $builder = $this->makeBuilder($data);
        $this->actWithOperator($builder, $conditions, $this->getOperatorHandler());
        $this->assertEquals($expectedCount, $builder->count());
    }

    /**
     * @param Condition[] $conditions
     * @param array<string, mixed> $data
     */
    public function orFilterable(array $conditions, array $data, int $expectedCount): void
    {
        $filterable = $this->makeFilterable($data);
        $this->expectException(Exception::class);
        $this->actWithOperator($filterable, $conditions, $this->getOperatorHandler());
    }

    abstract protected function getOperatorHandler(): Operator;
}
