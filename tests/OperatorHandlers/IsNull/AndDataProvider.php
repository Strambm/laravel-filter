<?php

namespace Cetria\Laravel\Filter\Tests\OperatorHandlers\IsNull;

use Cetria\Laravel\Filter\Operator;
use Cetria\Laravel\Filter\Condition;
use Cetria\Laravel\Filter\Tests\OperatorHandlers\DataProvider;

class AndDataProvider extends DataProvider
{
    public static function getData(): array
    {
        return [
            [
                [
                    new Condition('name', Operator::IS_NULL),
                    new Condition('price', Operator::IS_NULL)
                ], [
                    [
                        'name' => null,
                        'price' => 11
                    ], [
                        'name' => null,
                        'price' => null
                    ], [
                        'name' => 'a',
                        'price' => 12
                    ], [
                        'name' => '55',
                        'price' => null
                    ]
                ],
                1
            ]
        ];
    }
}
