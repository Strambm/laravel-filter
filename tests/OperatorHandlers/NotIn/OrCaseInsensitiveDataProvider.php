<?php

namespace Cetria\Laravel\Filter\Tests\OperatorHandlers\NotIn;

use Cetria\Laravel\Filter\Operator;
use Cetria\Laravel\Filter\Condition;
use Cetria\Laravel\Filter\Tests\OperatorHandlers\DataProvider;

class OrCaseInsensitiveDataProvider extends DataProvider
{
    public static function getData(): array
    {
        return [
            [
                [
                    new Condition('name', Operator::NOT_IN, ['a', 'b', 'c'], 'OR'),
                    new Condition('name', Operator::NOT_IN, ['D'], 'OR')
                ],
                [
                    [
                        'name' => 'a',
                    ],
                    [
                        'name' => 'b',
                    ],
                    [
                        'name' => 'B',
                    ],
                    [
                        'name' => 'c',
                    ],
                    [
                        'name' => 'd',
                    ]
                ],
                5
            ],
            [
                [
                    new Condition('name', Operator::NOT_IN, ['a', 'b', 2], 'OR'),
                    new Condition('name', Operator::NOT_IN, [2, 'B'], 'OR'),
                ],
                [
                    [
                        'name' => 'a',
                    ],
                    [
                        'name' => 'B',
                    ],
                    [
                        'name' => 1,
                    ],
                    [
                        'name' => 2,
                    ],
                ],
                2
            ],
            [
                [
                    new Condition('name', Operator::NOT_IN, ['a', 'ř'], 'OR'),
                    new Condition('name', Operator::NOT_IN, [1, 'Á' ], 'OR'),
                ],
                [
                    [
                        'name' => 'a',
                    ],
                    [
                        'name' => 'á',
                    ],
                    [
                        'name' => 'r',
                    ],
                    [
                        'name' => 'ř',
                    ],
                    [
                        'name' => 'Ř',
                    ],
                    [
                        'name' => 1,
                    ],
                ],
                6 //(1: r, 1, á || 2: a, ř, Ř) => a, á, r, ř, Ř, 1
            ]
        ];
    }
}