<?php

namespace Cetria\Laravel\Filter\Tests\OperatorHandlers\NotIn;

use Cetria\Laravel\Filter\Operator;
use Cetria\Laravel\Filter\Condition;
use Cetria\Laravel\Filter\Tests\OperatorHandlers\DataProvider;

class OrDataProvider extends DataProvider
{
    public static function getData(): array
    {
        return [
            [
                [
                    new Condition('price', Operator::NOT_IN, [1,2,3,4,5,6,7,8,9,10], 'OR'),
                    new Condition('price', Operator::NOT_IN, [5,6,7,8,9,10,11,12], 'OR')
                ],
                static::generateData('price', 1, 15),
                9
            ], [
                [
                    new Condition('name', Operator::NOT_IN, ['a', 'b', 'c'], 'OR'),
                    new Condition('name', Operator::NOT_IN, ['b', 'd'], 'OR')
                ], [
                    [
                        'name' => 'a',
                    ], [
                        'name' => 'b',
                    ], [
                        'name' => 'c',
                    ], [
                        'name' => 'd',
                    ]
                ],
                3
            ], [
                [
                    new Condition('name', Operator::NOT_IN, ['a', 1, 'c'], 'OR'),
                    new Condition('name', Operator::NOT_IN, ['b', 'd', 'c'], 'OR')
                ], [
                    [
                        'name' => 'a',
                    ], [
                        'name' => 'b',
                    ], [
                        'name' => 'c',
                    ], [
                        'name' => 'd',
                    ], [
                        'name' => 1,
                    ], [
                        'name' => 'test',
                    ]
                ],
                5 //(1: b,d,test || 2: a,1,test) => a,b,d,1,test
            ]
        ];
    }
}
