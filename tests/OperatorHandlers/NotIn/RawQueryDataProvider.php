<?php

namespace Cetria\Laravel\Filter\Tests\OperatorHandlers\NotIn;

use Cetria\Laravel\Filter\Operator;
use Cetria\Laravel\Filter\Condition;
use Cetria\Laravel\Filter\Tests\OperatorHandlers\DataProvider;

class RawQueryDataProvider extends DataProvider
{
    public static function getData(): array
    {
        return [
            [
                [
                    new Condition('nonexistentColumn', Operator::NOT_IN, [1,2,3,4,5,6,7,8,9,10]),
                ],
                static::generateData('price', 1, 15),
                \InvalidArgumentException::class
            ],
        ];
    }
}
