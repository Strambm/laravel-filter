<?php

namespace Cetria\Laravel\Filter\Tests\OperatorHandlers\NotIn;

use Cetria\Laravel\Filter\Operator;
use Cetria\Laravel\Filter\Condition;
use Cetria\Laravel\Filter\Tests\OperatorHandlers\DataProvider;

class AndCaseInsensitiveDataProvider extends DataProvider
{
    public static function getData(): array
    {
        return [
            [
                [
                    new Condition('name', Operator::NOT_IN, ['a', 'b']),
                    new Condition('name', Operator::NOT_IN, ['d'])
                ],
                [
                    [
                        'name' => 'a',
                    ],
                    [
                        'name' => 'b',
                    ],
                    [
                        'name' => 'B',
                    ],
                    [
                        'name' => 'c',
                    ],
                    [
                        'name' => 'd',
                    ]
                ],
                1
            ],
            [
                [
                    new Condition('name', Operator::NOT_IN, ['a', 'B', 'c']),
                ],
                [
                    [
                        'name' => 'a',
                    ],
                    [
                        'name' => 'b',
                    ],
                    [
                        'name' => 'B',
                    ],
                    [
                        'name' => 'c',
                    ],
                    [
                        'name' => 'd',
                    ]
                ],
                1
            ],
            [
                [
                    new Condition('name', Operator::NOT_IN, ['abc']),
                ],
                [
                    [
                        'name' => 'abc',
                    ],
                    [
                        'name' => 'aBc',
                    ],
                    [
                        'name' => 'ABC',
                    ],
                ],
                0
            ],
            [
                [
                    new Condition('name', Operator::NOT_IN, ['a', 'b', 2]),
                    new Condition('name', Operator::NOT_IN, [2, 'B']),
                ],
                [
                    [
                        'name' => 'a',
                    ],
                    [
                        'name' => 'B',
                    ],
                    [
                        'name' => 1,
                    ],
                    [
                        'name' => 2,
                    ],
                ],
                1
            ],
            [
                [
                    new Condition('name', Operator::NOT_IN, ['a', 'Ř', 1]),
                    new Condition('name', Operator::NOT_IN, ['Á']),
                ],
                [
                    [
                        'name' => 'a',
                    ],
                    [
                        'name' => 'á',
                    ],
                    [
                        'name' => 'r',
                    ],
                    [
                        'name' => 'ř',
                    ],
                    [
                        'name' => 1,
                    ],
                ],
                1
            ]
        ];
    }
}