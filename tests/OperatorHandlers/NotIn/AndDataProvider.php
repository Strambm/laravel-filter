<?php

namespace Cetria\Laravel\Filter\Tests\OperatorHandlers\NotIn;

use Cetria\Laravel\Filter\Operator;
use Cetria\Laravel\Filter\Condition;
use Cetria\Laravel\Filter\Tests\OperatorHandlers\DataProvider;

class AndDataProvider extends DataProvider
{
    public static function getData(): array
    {
        return [
            [
                [
                    new Condition('price', Operator::NOT_IN, [1,2,3,4,5,6,7,8,9,10]),
                    new Condition('price', Operator::NOT_IN, [5,6,7,8,9,10,11,12])
                ],
                static::generateData('price', 1, 15),
                3
            ], [
                [
                    new Condition('name', Operator::NOT_IN, ['a', 'b', 'c']),
                    new Condition('name', Operator::NOT_IN, ['b', 'd'])
                ], [
                    [
                        'name' => 'a',
                    ], [
                        'name' => 'b',
                    ], [
                        'name' => 'c',
                    ], [
                        'name' => 'd',
                    ]
                ],
                0
            ],
            [
                [
                    new Condition('name', Operator::NOT_IN, ['a', 1, 'c']),
                    new Condition('name', Operator::NOT_IN, ['b', 'd'])
                ], [
                    [
                        'name' => 'a',
                    ], [
                        'name' => 'b',
                    ], [
                        'name' => 'c',
                    ], [
                        'name' => 'd',
                    ], [
                        'name' => 1,
                    ], [
                        'name' => 2,
                    ]
                ],
                1 //1: b,d,2  && //2: a,c,1,2 => 2
            ]
        ];
    }
}
