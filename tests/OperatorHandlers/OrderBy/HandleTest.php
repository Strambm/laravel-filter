<?php

namespace Cetria\Laravel\Filter\Tests\OperatorHandlers\OrderBy;

use Exception;
use PHPUnit\Framework\Attributes\Test;
use Cetria\Laravel\Filter\OperatorHandlers\OrderBy;
use Cetria\Laravel\Filter\OperatorHandlers\Operator;
use PHPUnit\Framework\Attributes\DataProviderExternal;
use Cetria\Laravel\Filter\Tests\OperatorHandlers\Scope\Product; //ze Scope.php
use Cetria\Laravel\Filter\Tests\OperatorHandlers\HandleOperatorTestCase;

class HandleTest extends HandleOperatorTestCase
{
    #[Test]
    #[DataProviderExternal(AndDataProvider::class, 'getDataDesc')]
    public function andCollectionDesc(array $conditions, array $data, int $expectedFirstItemPrice): void
    {
        $collection = $this->makeColection($data);
        $this->actWithOperator($collection, $conditions, $this->getOperatorHandler());
        $firstItemPrice = $collection->first()->price;
        $this->assertEquals($expectedFirstItemPrice, $firstItemPrice);
    }

    #[Test]
    #[DataProviderExternal(AndDataProvider::class, 'getDataAsc')]
    public function andCollectionAsc(array $conditions, array $data, int $expectedFirstItemPrice): void
    {
        $collection = $this->makeColection($data);
        $this->actWithOperator($collection, $conditions, $this->getOperatorHandler());
        $firstItemPrice = $collection->first()->price;
        $this->assertEquals($expectedFirstItemPrice, $firstItemPrice);
    }

    #[Test]
    #[DataProviderExternal(AndDataProvider::class, 'getDataDesc')]
    public function andBuilderDesc(array $conditions, array $data, int $expectedFirstItemPrice): void
    {
        $builder = $this->makeBuilder($data);
        $this->actWithOperator($builder, $conditions, $this->getOperatorHandler());
        $firstItemPrice = $builder->get()->first()->price;
        $this->assertEquals($expectedFirstItemPrice, $firstItemPrice);
    }

    #[Test]
    #[DataProviderExternal(AndDataProvider::class, 'getDataAsc')]
    public function andBuilderAsc(array $conditions, array $data, int $expectedFirstItemPrice): void
    {
        $builder = $this->makeBuilder($data);
        $this->actWithOperator($builder, $conditions, $this->getOperatorHandler());
        $firstItemPrice = $builder->get()->first()->price;
        $this->assertEquals($expectedFirstItemPrice, $firstItemPrice);
    }

    #[Test]
    #[DataProviderExternal(AndDataProvider::class, 'getDataDesc')]
    public function andFilterableDesc(array $conditions, array $data, int $expectedFirstItemPrice): void
    {
        //$this->expectException(Exception::class);
        $filterable = $this->makeFilterable($data);
        $this->actWithOperator($filterable, $conditions, $this->getOperatorHandler());
        $firstItemPrice = $filterable->first()->price;
        $this->assertEquals($expectedFirstItemPrice, $firstItemPrice);
    }

    #[Test]
    #[DataProviderExternal(AndDataProvider::class, 'getDataAsc')]
    public function andFilterableAsc(array $conditions, array $data, int $expectedFirstItemPrice): void
    {
        //$this->expectException(Exception::class);
        $filterable = $this->makeFilterable($data);
        $this->actWithOperator($filterable, $conditions, $this->getOperatorHandler());
        $firstItemPrice = $filterable->first()->price;
        $this->assertEquals($expectedFirstItemPrice, $firstItemPrice);
    }


    /////////////////////////////////////////////////////////////////        OR tests:       ///////////////////////////////////////////////////////////////

    #[Test]
    #[DataProviderExternal(OrDataProvider::class, 'getDataDesc')]
    public function orCollectionDesc(array $conditions, array $data, int $expectedCount): void
    {
        $this->expectException(Exception::class);
        parent::orCollection($conditions, $data, $expectedCount);
    }

    #[Test]
    #[DataProviderExternal(OrDataProvider::class, 'getDataAsc')]
    public function orCollectionAsc(array $conditions, array $data, int $expectedCount): void
    {
        $this->expectException(Exception::class);
        parent::orCollection($conditions, $data, $expectedCount);
    }

    #[Test]
    #[DataProviderExternal(OrDataProvider::class, 'getDataDesc')]
    public function orBuilderDesc(array $conditions, array $data, int $expectedCount): void
    {
        $this->expectException(Exception::class);
        parent::orBuilder($conditions, $data, $expectedCount);
    }

    #[Test]
    #[DataProviderExternal(OrDataProvider::class, 'getDataAsc')]
    public function orBuilderAsc(array $conditions, array $data, int $expectedCount): void
    {
        $this->expectException(Exception::class);
        parent::orBuilder($conditions, $data, $expectedCount);
    }

    #[Test]
    #[DataProviderExternal(OrDataProvider::class, 'getDataAsc')]
    public function orFilterableAsc(array $conditions, array $data, int $expectedCount): void
    {
        $this->expectException(Exception::class);
        parent::orFilterable($conditions, $data, $expectedCount);
    }

    #[Test]
    #[DataProviderExternal(OrDataProvider::class, 'getDataDesc')]
    public function orFilterableDesc(array $conditions, array $data, int $expectedCount): void
    {
        $this->expectException(Exception::class);
        parent::orFilterable($conditions, $data, $expectedCount);
    }

    protected function getOperatorHandler(): Operator
    {
        return new OrderBy();
    }

    protected function getTestModelClass(): string
    {
        return Product::class;
    }
}
