<?php

namespace Cetria\Laravel\Filter\Tests\OperatorHandlers\OrderBy;

use Cetria\Laravel\Filter\Operator;
use Cetria\Laravel\Filter\Condition;
use Cetria\Laravel\Filter\Tests\OperatorHandlers\DataProvider;

class AndDataProvider extends DataProvider
{
    public static function getDataDesc(): array
    {
        $column = 'price';
        $arrayOfValues = (static::generateData($column, 1, 15)); // [1 - 15]
        $expectedfirstValue = 15;
        return [
            [
                [
                    new Condition($column, Operator::ORDER_BY, 'DESC', 'AND'),
                ],
                $arrayOfValues,
                $expectedfirstValue,
            ],
        ];
    }

    public static function getDataAsc(): array
    {
        $column = 'price';
        $arrayOfValues = (static::generateData($column, 1, 15));
        $reversedArrayOfValues = array_reverse($arrayOfValues); // [15 - 1]
        $expectedfirstValue = 1;
        return [
            [
                [
                    new Condition($column, Operator::ORDER_BY, 'ASC', 'AND'),
                ],
                $reversedArrayOfValues,
                $expectedfirstValue,
            ],
        ];
    }
}
