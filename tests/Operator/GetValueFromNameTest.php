<?php

namespace Cetria\Laravel\Filter\Tests\Operator;

use Cetria\Laravel\Filter\Operator;
use PHPUnit\Framework\Attributes\Test;
use ValueError;
use PHPUnit\Framework\TestCase;

class GetValueFromNameTest extends TestCase
{
    #[Test]
    public function badName(): void
    {
        $this->expectException(ValueError::class);
        Operator::getValueFromName('UndefinedName');
    }

    #[Test]
    public function correct(): void
    {
        $cases = Operator::cases();
        $operator = $cases[array_rand($cases)];
        $result = Operator::getValueFromName($operator->name);
        $this->assertEquals($operator->value, $result);
    }
}
