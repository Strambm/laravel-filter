<?php

namespace Cetria\Laravel\Filter\Tests\Filter;

use Exception;
use Cetria\Laravel\Filter\Condition;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\Attributes\DataProviderExternal;
use Cetria\Laravel\Filter\Tests\OperatorHandlers\Scope\Product;
use Cetria\Laravel\Filter\Tests\OperatorHandlers\OrderBy\AndDataProvider;
use Cetria\Laravel\Filter\Tests\OperatorHandlers\OrderBy\OrDataProvider;
use Cetria\Laravel\Filter\Tests\Filter\HandleFilterTestCase;

class FeatureOrderByApplyTest extends HandleFilterTestCase
{
    #[Test]
    #[DataProviderExternal(AndDataProvider::class, 'getDataAsc')]
    /**
     * @param Condition[] $conditions
     * @param array<string, mixed> $data
     */
    public function andCollectionAsc(array $conditions, array $data, int $expectedFirstItemPrice): void
    {
        $collection = $this->makeColection($data);
        $this->act($collection, $conditions);
        $firstItemPrice = $collection->first()->price;
        $this->assertEquals($expectedFirstItemPrice, $firstItemPrice);
    }

    #[Test]
    #[DataProviderExternal(AndDataProvider::class, 'getDataDesc')]
    /**
     * @param Condition[] $conditions
     * @param array<string, mixed> $data
     */
    public function andCollectionDesc(array $conditions, array $data, int $expectedFirstItemPrice): void
    {
        $collection = $this->makeColection($data);
        $this->act($collection, $conditions);
        $firstItemPrice = $collection->first()->price;
        $this->assertEquals($expectedFirstItemPrice, $firstItemPrice);
    }

    #[Test]
    #[DataProviderExternal(AndDataProvider::class, 'getDataAsc')]
    /**
     * @param Condition[] $conditions
     * @param array<string, mixed> $data
     */
    public function andBuilderAsc(array $conditions, array $data, int $expectedFirstItemPrice): void
    {
        $builder = $this->makeBuilder($data);
        $this->act($builder, $conditions);
        $firstItemPrice = $builder->get()->first()->price;
        $this->assertEquals($expectedFirstItemPrice, $firstItemPrice);
    }

    #[Test]
    #[DataProviderExternal(AndDataProvider::class, 'getDataDesc')]
    /**
     * @param Condition[] $conditions
     * @param array<string, mixed> $data
     */
    public function andBuilderDesc(array $conditions, array $data, int $expectedFirstItemPrice): void
    {
        $builder = $this->makeBuilder($data);
        $this->act($builder, $conditions);
        $firstItemPrice = $builder->get()->first()->price;
        $this->assertEquals($expectedFirstItemPrice, $firstItemPrice);
    }

    #[Test]
    #[DataProviderExternal(AndDataProvider::class, 'getDataAsc')]
    /**
     * @param Condition[] $conditions
     * @param array<string, mixed> $data
     */
    public function andFilterableAsc(array $conditions, array $data, int $expectedFirstItemPrice): void
    {
        //$this->expectException(Exception::class);
        $filterable = $this->makeFilterable($data);
        //dd($filterable->toSql());
        $this->act($filterable, $conditions);
        //dd($filterable);

        $firstItemPrice = $filterable->get()->first()->price;
        //dd($firstItemPrice, $filterable->toSql());
        $this->assertEquals($expectedFirstItemPrice, $firstItemPrice);
    }

    #[Test]
    #[DataProviderExternal(AndDataProvider::class, 'getDataDesc')]
    /**
     * @param Condition[] $conditions
     * @param array<string, mixed> $data
     */
    public function andFilterableDesc(array $conditions, array $data, int $expectedFirstItemPrice): void
    {
        //$this->expectException(Exception::class);
        $filterable = $this->makeFilterable($data);
        $this->act($filterable, $conditions);

        $firstItemPrice = $filterable->get()->first()->price;
        $this->assertEquals($expectedFirstItemPrice, $firstItemPrice);
    }

    /////////////////////////////////////////////////////////////////        OR tests:       ///////////////////////////////////////////////////////////////

    #[Test]
    #[DataProviderExternal(OrDataProvider::class, 'getDataAsc')]
    /**
     * @param Condition[] $conditions
     * @param array<string, mixed> $data
     */
    public function orCollectionAsc(array $conditions, array $data, int $expectedFirstItemPrice): void
    {
        $this->expectException(Exception::class);
        $collection = $this->makeColection($data);
        $this->act($collection, $conditions);
    }

    #[Test]
    #[DataProviderExternal(OrDataProvider::class, 'getDataDesc')]
    /**
     * @param Condition[] $conditions
     * @param array<string, mixed> $data
     */
    public function orCollectionDesc(array $conditions, array $data, int $expectedFirstItemPrice): void
    {
        $this->expectException(Exception::class);
        $collection = $this->makeColection($data);
        $this->act($collection, $conditions);
    }

    #[Test]
    #[DataProviderExternal(OrDataProvider::class, 'getDataAsc')]
    /**
     * @param Condition[] $conditions
     * @param array<string, mixed> $data
     */
    public function orBuilderAsc(array $conditions, array $data, int $expectedFirstItemPrice): void
    {
        $this->expectException(Exception::class);
        $builder = $this->makeBuilder($data);
        $this->act($builder, $conditions);
    }

    #[Test]
    #[DataProviderExternal(OrDataProvider::class, 'getDataDesc')]
    /**
     * @param Condition[] $conditions
     * @param array<string, mixed> $data
     */
    public function orBuilderDesc(array $conditions, array $data, int $expectedFirstItemPrice): void
    {
        $this->expectException(Exception::class);
        $builder = $this->makeBuilder($data);
        $this->act($builder, $conditions);
    }

    #[Test]
    #[DataProviderExternal(OrDataProvider::class, 'getDataAsc')]
    /**
     * @param Condition[] $conditions
     * @param array<string, mixed> $data
     */
    public function orFilterableAsc(array $conditions, array $data, int $expectedFirstItemPrice): void
    {
        $this->expectException(Exception::class);
        $filterable = $this->makeFilterable($data);
        $this->act($filterable, $conditions);
    }

    #[Test]
    #[DataProviderExternal(OrDataProvider::class, 'getDataDesc')]
    /**
     * @param Condition[] $conditions
     * @param array<string, mixed> $data
     */
    public function orFilterableDesc(array $conditions, array $data, int $expectedFirstItemPrice): void
    {
        $this->expectException(Exception::class);
        $filterable = $this->makeFilterable($data);
        $this->act($filterable, $conditions);
    }

    protected function getTestModelClass(): string
    {
        return Product::class;
    }
}
