<?php

namespace Cetria\Laravel\Filter\Tests\Filter;

use Exception;
use Cetria\Laravel\Filter\Condition;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\Attributes\DataProviderExternal;
use Cetria\Laravel\Filter\Tests\OperatorHandlers\Like\OrDataProvider;
use Cetria\Laravel\Filter\Tests\OperatorHandlers\Like\AndDataProvider;
use Cetria\Laravel\Filter\Tests\Filter\HandleFilterTestCase;

class FeatureLikeApplyTest extends HandleFilterTestCase
{
    #[Test]
    #[DataProviderExternal(AndDataProvider::class, 'getData')]
    /**
     * @param Condition[] $conditions
     * @param array<string, mixed> $data
     */
    public function andCollection(array $conditions, array $data, int $expectedCount): void
    {
        $collection = $this->makeColection($data);
        $this->act($collection, $conditions);
        $this->assertEquals($expectedCount, $collection->count());
    }

    #[Test]
    #[DataProviderExternal(AndDataProvider::class, 'getData')]
    /**
     * @param Condition[] $conditions
     * @param array<string, mixed> $data
     */
    public function andBuilder(array $conditions, array $data, int $expectedCount): void
    {
        $builder = $this->makeBuilder($data);
        $this->act($builder, $conditions);
        $this->assertEquals($expectedCount, $builder->count());
    }

    #[Test]
    #[DataProviderExternal(AndDataProvider::class, 'getData')]
    /**
     * @param Condition[] $conditions
     * @param array<string, mixed> $data
     */
    public function andFilterable(array $conditions, array $data, int $expectedCount): void
    {
        $filterable = $this->makeFilterable($data);
        $this->act($filterable, $conditions);
        $this->assertEquals($expectedCount, $filterable->count());
    }

    #[Test]
    #[DataProviderExternal(OrDataProvider::class, 'getData')]
    /**
     * @param Condition[] $conditions
     * @param array<string, mixed> $data
     */
    public function orCollection(array $conditions, array $data, int $expectedCount): void
    {
        $collection = $this->makeColection($data);
        $this->expectException(Exception::class);
        $this->act($collection, $conditions);
    }

    #[Test]
    #[DataProviderExternal(OrDataProvider::class, 'getData')]
    /**
     * @param Condition[] $conditions
     * @param array<string, mixed> $data
     */
    public function orBuilder(array $conditions, array $data, int $expectedCount): void
    {
        $builder = $this->makeBuilder($data);
        $this->act($builder, $conditions);
        $this->assertEquals($expectedCount, $builder->count());
    }

    #[Test]
    #[DataProviderExternal(OrDataProvider::class, 'getData')]
    /**
     * @param Condition[] $conditions
     * @param array<string, mixed> $data
     */
    public function orFilterable(array $conditions, array $data, int $expectedCount): void
    {
        $filterable = $this->makeFilterable($data);
        $this->act($filterable, $conditions);
        $this->assertEquals($expectedCount, $filterable->count());
    }
}
