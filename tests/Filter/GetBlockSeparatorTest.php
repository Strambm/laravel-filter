<?php

namespace Cetria\Laravel\Filter\Tests\Filter;

use PHPUnit\Framework\Test;
use PHPUnit\Framework\TestCase;
use Cetria\Laravel\Filter\Filter;
use Cetria\Helpers\Reflection\Reflection;

class GetBlockSeparatorTest extends TestCase
{
    #[Test]
    public function testMethod(): void
    {
        $methodName = 'getBlockSeparator';
        $method = Reflection::getHiddenMethod(Filter::class, $methodName);
        $this->assertEquals('_CON42_', $method->invoke(null, 42));
    }
}
