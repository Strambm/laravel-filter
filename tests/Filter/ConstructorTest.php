<?php

namespace Cetria\Laravel\Filter\Tests\Filter;

use Cetria\Helpers\Reflection\Reflection;
use Cetria\Laravel\Filter\Condition;
use Cetria\Laravel\Filter\Filter;
use Cetria\Laravel\Filter\Operator;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;

class ConstructorTest extends TestCase
{
    #[Test]
    public function empty(): void
    {
        $level = 8;
        $filter = new Filter(null, $level);
        $this->assertEquals($level, Reflection::getHiddenProperty($filter, 'level'));
    }

    #[Test]
    public function fromQueryString(): void
    {
        $filterSource = new Filter();
        $filterSource->addScope(new Condition('test', Operator::EQ, 8));
        
        $filter = new Filter((string) $filterSource);

        $this->assertEquals($filterSource, $filter);
        $this->assertNotEquals(new Filter(), $filter);
    }
}
