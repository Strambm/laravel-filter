<?php

namespace Cetria\Laravel\Filter\Tests\Filter;

use Cetria\Laravel\Filter\Tests\TestCase;
use Cetria\Laravel\Filter\Condition;
use Cetria\Helpers\Reflection\Reflection;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Cetria\Laravel\Filter\Eloquent\Relations\FilterableInterface;
use Cetria\Laravel\Filter\Filter;

abstract class HandleFilterTestCase extends TestCase
{
   /**
     * @param Condition[] $conditions
     */
    protected function act(Builder|Collection|FilterableInterface &$source, array $conditions): void
    {
        $filter = new Filter();

        foreach($conditions as $condition) {
            $filter->addScope($condition);
        }
        $filter->apply($source);
    }

}
