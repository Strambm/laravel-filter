<?php

namespace Cetria\Laravel\Filter\Tests\Filter;

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use Cetria\Laravel\Filter\Filter;
use Cetria\Laravel\Filter\Operator;
use Cetria\Laravel\Filter\Condition;
use PHPUnit\Framework\Attributes\Test;

class GetLogicalOperatorTest extends TestCase
{
    #[Test]
    public function empty(): void
    {
        $filter = new Filter();
        $this->assertEquals('AND', $filter->getLogicalOperator());
    }

    #[Test]
    #[DataProvider('withOperatorDataProvider')]
    public function withOperator(string $operator): void
    {
        $filter = new Filter();
        $filter->addScope(new Condition('test', Operator::EQ, 1, $operator));
        $this->assertEquals($operator, $filter->getLogicalOperator());
    }

    public static function withOperatorDataProvider(): array
    {
        return [
            [
                'AND'
            ], [
                'OR'
            ]
        ];
    }
}
