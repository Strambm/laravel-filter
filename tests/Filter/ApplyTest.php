<?php

namespace Cetria\Laravel\Filter\Tests\Filter;

use Cetria\Laravel\Filter\Eloquent\Relations\BelongsToMany;
use Cetria\Laravel\Filter\Eloquent\Relations\FilterableInterface;
use Cetria\Laravel\Helpers\Test\Dummy\Category;
use Exception;
use PHPUnit\Framework\TestCase;
use Cetria\Laravel\Filter\Filter;
use Cetria\Laravel\Filter\Condition;
use Cetria\Laravel\Filter\Operator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Cetria\Laravel\Helpers\Test\Dummy\Product;
use Cetria\Laravel\Filter\Traits\ExtensionForModel;
use Cetria\Laravel\Helpers\Test\Dummy\DummyHelper;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\Attributes\DataProvider;

class ApplyTest extends TestCase
{
    #[Test]
    #[DataProvider('andDataProvider')]
    public function andCollection(Filter $filter, array $modelsParams, int $expectedCount): void
    {
        $collection = $this->arrangeCollection($modelsParams);
        $result = $filter->apply($collection);

        $this->assertCount($expectedCount, $result);
        $this->assertCount($expectedCount, $collection);
    }

    #[Test]
    #[DataProvider('orDataProvider')]
    public function orCollection(Filter $filter, array $modelsParams, int $expecteCount): void
    {
        $this->expectException(Exception::class);
        $collection = $this->arrangeCollection($modelsParams);
        $filter->apply($collection);
    }

    #[Test]
    #[DataProvider('andDataProvider')]
    public function andBuilder(Filter $filter, array $modelsParams, int $expectedCount): void
    {
        DummyHelper::init();
        $builder = $this->arraneBuilder($modelsParams);
        $result = $filter->apply($builder);
        $this->assertEquals($expectedCount, $builder->count());
        $this->assertEquals($expectedCount, $result->count());
    }

    #[Test]
    #[DataProvider('orDataProvider')]
    public function orBuilder(Filter $filter, array $modelsParams, int $expectedCount): void
    {
        DummyHelper::init();
        $builder = $this->arraneBuilder($modelsParams);
        $result = $filter->apply($builder);
        $this->assertEquals($expectedCount, $builder->count());
        $this->assertEquals($expectedCount, $result->count());
    }

    #[Test]
    #[DataProvider('andDataProvider')]
    public function andRelation(Filter $filter, array $modelsParams, int $expectedCount): void
    {
        DummyHelper::init();
        $relation = $this->arrangeRelation($modelsParams);
        $result = $filter->apply($relation);
        $this->assertEquals($expectedCount, $result->count());
        $this->assertEquals($expectedCount, $relation->count());
    }

    #[Test]
    #[DataProvider('orDataProvider')]
    public function orRelation(Filter $filter, array $modelsParams, int $expectedCount): void
    {
        DummyHelper::init();
        $relation = $this->arrangeRelation($modelsParams);
        $result = $filter->apply($relation);
        $this->assertEquals($expectedCount, $relation->count());
        $this->assertEquals($expectedCount, $result->count());
    }

    public static function andDataProvider(): array
    {
        return [
            [
                'filter' => (new Filter())
                    ->addScope(new Condition('price', Operator::NEQ, 20))
                    ->addScope(
                        (new Filter())
                            ->addScope(new Condition('price', Operator::LTE, 40))
                            ->addScope(new Condition('price', Operator::GTE, 5))
                    ),
                'modelsParams' => [
                    [
                        'price' => 20,
                    ], [
                        'price' => 50,
                    ], [
                        'price' => 1,
                    ], [
                        'price' => 8,
                    ], [
                        'price' => 39
                    ],
                ],
                'expectedCount' =>2,
            ]
        ];
    }

    public static function orDataProvider(): array
    {
        return [
            [
                (new Filter())
                    ->addScope(new Condition('price', Operator::EQ, 20, 'OR'))
                    ->addScope(
                        (new Filter())
                            ->addScope(new Condition('price', Operator::LT, 11))
                            ->addScope(new Condition('price', Operator::GT, 7))
                    ),
                [
                    [
                        'price' => 20,
                    ], [
                        'price' => 11,
                    ], [
                        'price' => 1,
                    ], [
                        'price' => 8,
                    ], [
                        'price' => 7
                    ], [
                        'price' => 9
                    ],
                ],
                0,
            ], [
                (new Filter())
                    ->addScope(new Condition('price', Operator::EQ, 20, 'OR'))
                    ->addScope(
                        (new Filter())
                            ->addScope(new Condition('price', Operator::LT, 11, 'OR'))
                            ->addScope(new Condition('price', Operator::GT, 7))
                    ),
                [
                    [
                        'price' => 20,
                    ], [
                        'price' => 11,
                    ], [
                        'price' => 1,
                    ], [
                        'price' => 8,
                    ], [
                        'price' => 7
                    ], [
                        'price' => 9
                    ],
                ],
                3,
            ]
        ];
    }

    protected function arrangeCollection(array $params): Collection
    {
        $colection = new Collection();
        foreach($params as $paramsForModel) {
            $model = $this->arrangeModel();
            foreach($paramsForModel as $key => $value) {
                $model->$key = $value;
            }
            $colection->push($model);
        }
        return $colection;
    }

    protected function arrangeRelation(array $params): FilterableInterface
    {
        $category = Category::factory()
            ->create();
        foreach($params as $paramsForModel) {
            $product = Product::factory()->create($paramsForModel);
            $product->categories()->save($category);
        }
        return new BelongsToMany((new Product())->newQuery(), $category, 'category_product', 'category_id', 'product_id', 'id', 'id');
    }

    protected function arraneBuilder(array $params): Builder
    {
        foreach($params as $paramsForModel) {
            Product::factory()->create($paramsForModel);
        }
        return $this->arrangeModel()->query();
    }

    protected function arrangeModel(): Model
    {
        $model = new class extends Product
        {
            use ExtensionForModel;
        };
        return $model;
    }
}
