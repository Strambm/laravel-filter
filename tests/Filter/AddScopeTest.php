<?php

namespace Cetria\Laravel\Filter\Tests\Filter;

use Cetria\Helpers\Reflection\Reflection;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Cetria\Laravel\Filter\Filter;
use Cetria\Laravel\Filter\Condition;
use Cetria\Laravel\Filter\FilterEntity;
use Cetria\Laravel\Filter\Operator;

class AddScopeTest extends TestCase
{
    #[Test]
    public function condition(): void
    {
        $filter = new Filter();
        $condition = new Condition('test', Operator::EQ, 12);
        
        $ressult = $filter->addScope($condition);

        $this->assertEquals($filter, $ressult);
        $scopes = Reflection::getHiddenProperty($ressult, 'scopes');
        $this->assertCount(1, $scopes);
        $this->assertEquals($condition, $scopes[0]);
    }

    #[Test]
    public function filter(): void
    {
        $filterN1 = new Filter();
        $filterN2 = new Filter();
        $filterN3 = new Filter();
        $condition = new Condition('test', Operator::EQ, 12);

        $filterN3->addScope($condition);
        $filterN2->addScope($filterN3);
        $filterN1->addScope($filterN2);

        $resultFilterN2 = $this->assertFilterDimension($filterN1, Filter::class, 1);
        $resultFilterN3 = $this->assertFilterDimension($resultFilterN2, Filter::class, 2);
        $resultCondition = $this->assertFilterDimension($resultFilterN3, Condition::class, 3);
        $this->assertEquals($condition, $resultCondition);
    }

    protected function assertFilterDimension(Filter $filter, string $expectedChildClass, int $expectedLevel): FilterEntity
    {
        $scopes = Reflection::getHiddenProperty($filter, 'scopes');
        $this->assertCount(1, $scopes);
        $this->assertInstanceOf($expectedChildClass, $scopes[0]);
        $this->assertEquals($expectedLevel, Reflection::getHiddenProperty($filter, 'level'));
        return $scopes[0];
    }
}
